from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.models import Group
from .models import *
from .forms import UserAdminCreationForm, UserAdminChangeForm

class UserAdmin(BaseUserAdmin):
		form = UserAdminChangeForm
		add_form = UserAdminCreationForm
		search_fields = ['username']
		list_display = ('username', 'first_name', 'last_name', 'admin')
		list_filter = ('admin',)
		fieldsets = (
				(None, 
					{'fields': ('username', 'password',)}
				),
				('Personal Information', 
					{'fields': ('first_name', 'last_name', 'email', )}
				),
				('Permissions', 
					{'fields': ('admin', 'staff', 'active')}
				),
		)
		add_fieldsets = ( 
				(None,  
						{ 'classes' : ('wide'),
						'fields'  : ('username',  'password1', 'password2',)
						}
				),
				('Basic Information', 
						{ 'fields' : ('first_name', 'last_name', 'email')
						}
				),
				('Permissions', 
						{ 'fields' : ('admin', 'staff', 'active',)
						}
				),
		)
		ordering = ['username']
		filter_horizontal = ()

admin.site.register(User, UserAdmin)
admin.site.unregister(Group)

admin.site.register(Location)
admin.site.register(NetworkDevice)
admin.site.register(Sfp)
admin.site.register(Computer)
admin.site.register(OtherSoftware)
admin.site.register(MicrosoftOffice)
admin.site.register(OperatingSystem)
admin.site.register(Monitor)
admin.site.register(Printer)
admin.site.register(Scanner)
admin.site.register(Backup)