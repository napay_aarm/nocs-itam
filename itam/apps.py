from django.apps import AppConfig
class ItamConfig(AppConfig):
    name = 'itam'

    def ready(self):
        import itam.signals
        from actstream import registry
        registry.register(self.get_model('User'))
        registry.register(self.get_model('Computer'))
        registry.register(self.get_model('Monitor'))
        registry.register(self.get_model('Printer'))
        registry.register(self.get_model('Scanner'))
        registry.register(self.get_model('NetworkDevice'))
        registry.register(self.get_model('Sfp'))
        registry.register(self.get_model('OtherSoftware'))
        registry.register(self.get_model('MicrosoftOffice'))
        registry.register(self.get_model('OperatingSystem'))
        registry.register(self.get_model('Location'))
