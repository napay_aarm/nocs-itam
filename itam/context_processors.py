from notifications.models import Notification

def assets_count(request):
	if request.user.is_authenticated:
		unread_notifications = Notification.objects.filter(recipient=request.user, unread=True)	
		return {"unread_notifications": unread_notifications}
	else:
		unread_notifications = Notification.objects.filter(recipient=request.user.is_authenticated, unread=True)	
		return {"unread_notifications": unread_notifications}