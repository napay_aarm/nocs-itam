from django import forms
from django.contrib.auth.forms import ReadOnlyPasswordHashField, AuthenticationForm
from django.contrib.auth import authenticate
from .models import *
from django.db.models import Q
from django.conf import settings
from .widgets import RelatedFieldWidgetCanAdd

class RegisterForm(forms.ModelForm):
	password1 = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control'}), label="Password")
	password2 = forms.CharField(label='Confirm Password', widget=forms.PasswordInput(attrs={'class': 'form-control'}))
	username = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}))
	email = forms.EmailField(widget=forms.EmailInput(attrs={'class': 'form-control'}))
	first_name = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}))
	last_name = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}))

	class Meta:
		model = User
		fields = ['username', 'email', 'first_name', 'last_name', 'active', 'admin' ]

	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)
		self.fields['active'].help_text = 'Designates whether the user is active and can log in. Uncheck this instead of deleting accounts.'
		self.fields['admin'].help_text = 'Designates whether this user has all privileges.'
		self.fields['password1'].help_text = 'Your password cannot be too similar to your other personal information, must contain at least 8 characters, cannot be a commonly used password, and cannot be entirely numeric.'

	def clean_username(self):
		username = self.cleaned_data.get("username")
		qs = User.objects.filter(username=username)
		if qs.exists():
			raise forms.ValidationError("Username already exists!")
		return username

	def clean_password2(self):
		password1 = self.cleaned_data.get("password1")
		password2 = self.cleaned_data.get("password2")
		if password1 != password2:
			raise forms.ValidationError("Passwords do not match!")
		return password2

	def save(self, commit=True):
		user = super(RegisterForm, self).save(commit=False)
		user.set_password(self.cleaned_data["password1"])
		if commit:
			user.save()
		return user

class UserAdminCreationForm(forms.ModelForm):
	password1 = forms.CharField(label='Password', widget=forms.PasswordInput)
	password2 = forms.CharField(label='Password confirmation', widget=forms.PasswordInput)
	username = forms.CharField(widget=forms.TextInput)
	email = forms.EmailField(widget=forms.EmailInput)
	first_name = forms.CharField(widget=forms.TextInput)
	last_name = forms.CharField(widget=forms.TextInput)

	class Meta:
		model = User
		fields = ['username', 'email', 'first_name', 'last_name', 'admin', 'staff']

	def clean_username(self):
		username = self.cleaned_data.get("username")
		qs = User.objects.filter(username=username)
		if qs.exists():
			raise forms.ValidationError("Username already exists!")
		return username 

	def clean_password2(self):
		password1 = self.cleaned_data.get("password1")
		password2 = self.cleaned_data.get("password2")
		if password1 != password2:
			raise forms.ValidationError("Passwords do not match!")
		return password2

	def save(self, commit=True):
		user = super(UserAdminCreationForm, self).save(commit=False)
		user.set_password(self.cleaned_data["password1"])
		if commit:
			user.save()
		return user

class UserAdminChangeForm(forms.ModelForm):
	password = ReadOnlyPasswordHashField()

	class Meta:
		model = User
		fields = ['username', 'password', 'email', 'first_name', 'last_name', 'active', 'staff', 'admin']

	def clean_password(self):
		return self.initial["password"]

class UserLoginForm(forms.Form):
	username = 	forms.CharField(required=True, label='Username',widget=forms.TextInput(attrs={'class':'form-control', 'placeholder':'Username', 'autofocus': True}))
	password = forms.CharField(required=True, label='Password', widget=forms.PasswordInput(attrs={'class':'form-control', 'placeholder':'Password', }))

	def clean(self, *args, **kwargs):
		username = self.cleaned_data.get("username")
		password = self.cleaned_data.get("password")
		user = authenticate(username=username, password=password)
		user_qs = User.objects.filter(username=username)
		if user_qs.count() == 1:
			user = user_qs.first()
		if not user:
			raise forms.ValidationError("Incorrect username or password")
		if not user.check_password(password):
			raise forms.ValidationError("Incorrect username or password")
		if not user.is_active:
			raise forms.ValidationError("This user is no longer active")
		return super(UserLoginForm, self).clean(*args, **kwargs)

class UserProfileChangeForm(forms.ModelForm):
	password = ReadOnlyPasswordHashField()
	username = forms.CharField(required=True, widget=forms.TextInput(attrs={'class': 'form-control'}))
	email = forms.EmailField(required=True, widget=forms.TextInput(attrs={'class': 'form-control'}))
	first_name = forms.CharField(required=True, widget=forms.TextInput(attrs={'class': 'form-control'}))
	last_name = forms.CharField(required=True, widget=forms.TextInput(attrs={'class': 'form-control'}))
	active = forms.BooleanField(required=False, widget=forms.CheckboxInput(attrs={'class': 'mt-2'}))
	admin = forms.BooleanField(required=False, widget=forms.CheckboxInput(attrs={'class': 'mt-2'}))

	class Meta:
		model = User
		exclude = ['staff', 'last_login']

	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)
		self.fields['active'].help_text = 'Designates whether the user is active and can log in. Uncheck this instead of deleting accounts.'
		self.fields['admin'].help_text = 'Designates whether this user has all privileges.'

	def clean_password(self):
		return self.initial["password"]

class ComputerForm(forms.ModelForm):
	computer_id = forms.CharField(widget=forms.TextInput, label="Computer ID")
	hardware_type = forms.ChoiceField(widget=forms.Select, label="Hardware Type", choices=Computer.HTYPE)
	monitor = forms.ModelChoiceField(empty_label="Choose...", required=False, queryset=Monitor.objects.filter(is_archived=False, computer__isnull=True), widget=RelatedFieldWidgetCanAdd(Monitor, "itam:add-monitor-popup"))
	scanner = forms.ModelChoiceField(empty_label="Choose...", required=False, queryset=Scanner.objects.filter(is_archived=False, computer__isnull=True), widget=RelatedFieldWidgetCanAdd(Scanner, "itam:add-scanner-popup"))
	printer = forms.ModelChoiceField(empty_label="Choose...", required=False, queryset=Printer.objects.filter(is_archived=False, computer__isnull=True), widget=RelatedFieldWidgetCanAdd(Printer, "itam:add-printer-popup"))
	system_specs = forms.CharField(widget=forms.Textarea, label="System Specifications")
	base_license_os = forms.CharField(required=False, widget=forms.TextInput, label="Base License OS")
	operating_system = forms.ModelChoiceField(empty_label="Choose...", required=False, queryset=OperatingSystem.objects.filter(is_archived=False, computer__isnull=True), 
		widget=RelatedFieldWidgetCanAdd(OperatingSystem, "itam:add-os-popup"), label="Operating System")
	microsoft_office = forms.ModelChoiceField(empty_label="Choose...", required=False, queryset=MicrosoftOffice.objects.filter(is_archived=False, computer__isnull=True), 
		widget=RelatedFieldWidgetCanAdd(MicrosoftOffice, "itam:add-mo-popup"), label="Microsoft Office")
	spss = forms.CharField(required=False, widget=forms.TextInput, label="SPSS")
	libre_office = forms.CharField(required=False, widget=forms.TextInput, label="Libre Office")
	open_office = forms.CharField(required=False, widget=forms.TextInput, label="Open Office")
	other_software = forms.ModelChoiceField(empty_label="Choose...", required=False, queryset=OtherSoftware.objects.filter(is_archived=False, computer__isnull=True),
		widget=RelatedFieldWidgetCanAdd(OtherSoftware, "itam:add-other-popup"), label="Other Software")
	location = forms.ModelChoiceField(empty_label="Choose...", required=False, queryset= Location.objects.all(), widget=RelatedFieldWidgetCanAdd(Location, "itam:add-location-popup"))
	deployment_date = forms.DateField(required=False, widget=forms.DateInput(attrs={'type':'date'}), label="Deployment Date")
	life_span = forms.IntegerField(required=False, widget=forms.NumberInput, label="Life Span")
	is_server = forms.BooleanField(required=False, widget=forms.CheckboxInput, label="Server")

	class Meta:
		model = Computer
		exclude = ['deployment_status', 'termination_status', 'qrcode', 'termination_date', 'is_archived']

	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)
		self.fields['computer_id'].help_text = 'Example: (DSSD_D001)'
		self.fields['hardware_type'].help_text = 'Select a computer hardware type.'
		self.fields['monitor'].help_text = 'Select from a list of undeployed Monitors.'
		self.fields['scanner'].help_text = 'Select from a list of undeployed Scanner.'
		self.fields['printer'].help_text = 'Select from a list of undeployed Printers.'
		self.fields['base_license_os'].help_text = 'Enter the base license operating system.'
		self.fields['operating_system'].help_text = 'Select from a list of Operating Systems.'
		self.fields['microsoft_office'].help_text = 'Select from a list of Microsoft Offices.'
		self.fields['spss'].help_text = 'SPSS Statistics software package'
		self.fields['libre_office'].help_text = 'Enter the installed Libre Office of this Computer.'
		self.fields['open_office'].help_text = 'Enter the installed Open Office of this Computer.'
		self.fields['other_software'].help_text = 'Select from a list of Other Licensed Software.'
		self.fields['location'].help_text = 'Adding a location will set the Deployment Status to "Deployed"'
		self.fields['life_span'].help_text = 'Enter the life span of this Computer in YEARS. (e.g. 5)'
		self.fields['is_server'].help_text = 'Select if the Computer is a server'
		self.fields['deployment_date'].help_text = 'Date of deployment. (Note:  Deployment date + Life Span = Termination date)'
		self.fields['computer_id'].widget.attrs.update({'class': 'form-control', 'style': 'width: 90%;'})
		self.fields['hardware_type'].widget.attrs.update({'class': 'form-control custom-select', 'style': 'width: 90%;'})
		self.fields['monitor'].widget.attrs.update({'class': 'form-control custom-select', 'style': 'width: 90%;'})
		self.fields['scanner'].widget.attrs.update({'class': 'form-control custom-select', 'style': 'width: 90%;'})
		self.fields['printer'].widget.attrs.update({'class': 'form-control custom-select', 'style': 'width: 90%;'})
		self.fields['system_specs'].widget.attrs.update({'class': 'form-control', 'style': 'width: 90%;'})
		self.fields['base_license_os'].widget.attrs.update({'class': 'form-control', 'style': 'width: 90%;'})
		self.fields['operating_system'].widget.attrs.update({'class': 'form-control custom-select', 'style': 'width: 90%;'})
		self.fields['microsoft_office'].widget.attrs.update({'class': 'form-control custom-select', 'style': 'width: 90%;'})
		self.fields['spss'].widget.attrs.update({'class': 'form-control', 'style': 'width: 90%;'})
		self.fields['libre_office'].widget.attrs.update({'class': 'form-control', 'style': 'width: 90%;'})
		self.fields['open_office'].widget.attrs.update({'class': 'form-control', 'style': 'width: 90%;'})
		self.fields['other_software'].widget.attrs.update({'class': 'form-control custom-select', 'style': 'width: 90%;'})
		self.fields['location'].widget.attrs.update({'class': 'form-control custom-select mr-0', 'style': 'width: 90%;'})
		self.fields['deployment_date'].widget.attrs.update({'class': 'form-control', 'style': 'width: 90%;'})
		self.fields['life_span'].widget.attrs.update({'class': 'form-control', 'min': '1', 'max': '10', 'style': 'width: 90%;', 'pattern': '[0-9]', 'maxlength': '2'})
		self.fields['is_server'].widget.attrs.update({'class': 'mt-2'})
		if self.instance.monitor:
			self.fields['monitor'].queryset = Monitor.objects.filter(Q(is_archived=False, computer__isnull=True) | Q(computer__monitor=self.instance.monitor))
		if self.instance.scanner:
			self.fields['scanner'].queryset = Scanner.objects.filter(Q(is_archived=False, computer__isnull=True) | Q(computer__scanner=self.instance.scanner))
		if self.instance.printer:
			self.fields['printer'].queryset = Printer.objects.filter(Q(is_archived=False, computer__isnull=True) | Q(computer__printer=self.instance.printer))
		if self.instance.operating_system:
			self.fields['operating_system'].queryset = OperatingSystem.objects.filter(Q(is_archived=False, computer__isnull=True) | Q(computer__operating_system=self.instance.operating_system))
		if self.instance.microsoft_office:
			self.fields['microsoft_office'].queryset = MicrosoftOffice.objects.filter(Q(is_archived=False, computer__isnull=True) | Q(computer__microsoft_office=self.instance.microsoft_office))
		if self.instance.other_software:
			self.fields['other_software'].queryset = OtherSoftware.objects.filter(Q(is_archived=False, computer__isnull=True) | Q(computer__other_software=self.instance.other_software))

class SfpForm(forms.ModelForm):
	product_number = forms.CharField(widget=forms.TextInput, label="Product Number")
	manufacturer = forms.CharField(widget=forms.TextInput)
	serial_number = forms.CharField(widget=forms.TextInput, label="Serial Number")
	product_description = forms.CharField(widget=forms.Textarea, label="Product Description")
	purchase_order = forms.CharField(widget=forms.TextInput, label="Purchase Order")
	vendor = forms.CharField(widget=forms.TextInput)
	purchase_date = forms.DateField(widget=forms.DateInput(attrs={'type':'date'}), label="Purchase Date")
	deployment_date = forms.DateField(required=False, widget=forms.DateInput(attrs={'type':'date'}), label="Deployment Date")
	life_span = forms.CharField(required=False, widget=forms.NumberInput, label="Life Span")
	network_device = forms.ModelChoiceField(required=False, queryset=NetworkDevice.objects.filter(is_archived=False, category="Switch", deployment_status="Deployed"), widget=RelatedFieldWidgetCanAdd(NetworkDevice, "itam:add-network-device-popup"), label="Switch")

	class Meta:
		model = Sfp
		exclude = ['deployment_status', 'termination_status', 'qrcode', 'termination_date', 'is_archived', 'location']

	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)
		self.fields['deployment_date'].help_text = 'Date of deployment. (Note:  Deployment date + Life Span = Termination date)'
		self.fields['life_span'].help_text = 'Enter the life span of this Sfp in YEARS. (e.g. 5)'
		self.fields['purchase_date'].help_text = 'Date of purchase'
		self.fields['product_number'].help_text = 'Example: GLC-LH-SMD'
		self.fields['network_device'].help_text = 'Add a sfp to a Switch.'
		self.fields['network_device'].widget.attrs.update({'class': 'form-control custom-select', 'style': 'width: 90%;'})
		self.fields['product_number'].widget.attrs.update({'class': 'form-control', 'style': 'width: 90%;'})
		self.fields['manufacturer'].widget.attrs.update({'class': 'form-control', 'style': 'width: 90%;'})
		self.fields['serial_number'].widget.attrs.update({'class': 'form-control', 'style': 'width: 90%;'})
		self.fields['product_description'].widget.attrs.update({'class': 'form-control', 'style': 'width: 90%;'})
		self.fields['purchase_order'].widget.attrs.update({'class': 'form-control', 'style': 'width: 90%;'})
		self.fields['vendor'].widget.attrs.update({'class': 'form-control', 'style': 'width: 90%;'})
		self.fields['purchase_date'].widget.attrs.update({'class': 'form-control', 'style': 'width: 90%;'})
		self.fields['deployment_date'].widget.attrs.update({'class': 'form-control', 'style': 'width: 90%;'})
		self.fields['life_span'].widget.attrs.update({'class': 'form-control', 'min': '1', 'max': '10', 'style': 'width: 90%;'})
		if self.instance.network_device:
			self.fields['network_device'].queryset = NetworkDevice.objects.filter(Q(is_archived=False, category="Switch") | Q(sfp__network_device=self.instance.network_device))

class NetworkDeviceForm(forms.ModelForm):
	category = forms.CharField(widget=forms.TextInput)
	code = forms.CharField(widget=forms.TextInput)
	manufacturer = forms.CharField(widget=forms.TextInput)
	details = forms.CharField(widget=forms.TextInput)
	primary_firmware = forms.CharField(widget=forms.TextInput, label="Primary Firmware")
	secondary_firmware = forms.CharField(widget=forms.TextInput, label="Secondary Firmware")
	management_ip = forms.CharField(widget=forms.TextInput, label="Management IP")
	purchase_order = forms.CharField(widget=forms.TextInput)
	purchase_date = forms.DateField(widget=forms.DateInput(attrs={'type':'date'}), label="Purchase Date")
	vendor = forms.CharField(widget=forms.TextInput)
	acquisition_price = forms.DecimalField(widget=forms.NumberInput, label="Acquisition Price")
	warranty_date = forms.DateField(widget=forms.DateInput(attrs={'type':'date'}), label="Warranty Date")
	serial_number = forms.CharField(widget=forms.TextInput, label="Serial Number")
	mac_address = forms.CharField(widget=forms.TextInput, label="MAC Address")
	poe_status = forms.CharField(required=False, widget=forms.TextInput, label="POE Status")
	location = forms.ModelChoiceField(empty_label="Choose...", required=False, queryset= Location.objects.all(), widget=RelatedFieldWidgetCanAdd(Location, "itam:add-location-popup"))
	deployment_date = forms.DateField(required=False, widget=forms.DateInput(attrs={'type':'date'}), label="Deployment Date")
	life_span = forms.IntegerField(required=False, widget=forms.NumberInput, label="Life Span")

	class Meta:
		model = NetworkDevice
		exclude = ['deployment_status', 'termination_status', 'qrcode', 'termination_date', 'is_archived']

	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)
		self.fields['category'].help_text = 'Examples: Switch, Router.'
		self.fields['code'].help_text = 'Enter the name of the Device (e.g. SG500-52P).'
		self.fields['management_ip'].help_text = 'Example: 192.168.0.1.'
		self.fields['mac_address'].help_text = 'Use a colon as the delimiter. (Format: MM:MM:MM:SS:SS:SS).'
		self.fields['poe_status'].help_text = 'Power over Ethernet Status.'
		self.fields['location'].help_text = 'Adding a location will set the Deployment Status to "Deployed".'
		self.fields['deployment_date'].help_text = 'Date of deployment.'
		self.fields['life_span'].help_text = 'Enter the life span of this Device in YEARS. (e.g. 5).'
		self.fields['warranty_date'].help_text = 'End of Warranty.'
		self.fields['category'].widget.attrs.update({'class': 'form-control', 'style': 'width: 90%;'})
		self.fields['code'].widget.attrs.update({'class': 'form-control', 'style': 'width: 90%;'})
		self.fields['manufacturer'].widget.attrs.update({'class': 'form-control', 'style': 'width: 90%;'})
		self.fields['details'].widget.attrs.update({'class': 'form-control', 'style': 'width: 90%;'})
		self.fields['primary_firmware'].widget.attrs.update({'class': 'form-control', 'style': 'width: 90%;'})
		self.fields['secondary_firmware'].widget.attrs.update({'class': 'form-control', 'style': 'width: 90%;'})
		self.fields['management_ip'].widget.attrs.update({'class': 'form-control', 'style': 'width: 90%;'})
		self.fields['purchase_order'].widget.attrs.update({'class': 'form-control', 'style': 'width: 90%;'})
		self.fields['purchase_date'].widget.attrs.update({'class': 'form-control', 'style': 'width: 90%;'})
		self.fields['vendor'].widget.attrs.update({'class': 'form-control', 'style': 'width: 90%;'})
		self.fields['acquisition_price'].widget.attrs.update({'class': 'form-control', 'style': 'width: 90%;'})
		self.fields['warranty_date'].widget.attrs.update({'class': 'form-control', 'style': 'width: 90%;'})
		self.fields['serial_number'].widget.attrs.update({'class': 'form-control', 'style': 'width: 90%;'})
		self.fields['mac_address'].widget.attrs.update({'class': 'form-control', 'style': 'width: 90%;'})
		self.fields['poe_status'].widget.attrs.update({'class': 'form-control', 'maxlength': '5', 'style': 'width: 90%;'})
		self.fields['location'].widget.attrs.update({'class': 'form-control custom-select', 'style': 'width: 90%;'})
		self.fields['deployment_date'].widget.attrs.update({'class': 'form-control', 'style': 'width: 90%;'})
		self.fields['life_span'].widget.attrs.update({'class': 'form-control', 'min': '1', 'max': '10', 'style': 'width: 90%;'})

class OperatingSystemForm(forms.ModelForm):
	os_name = forms.CharField(widget=forms.TextInput, label="Name")
	os_type = forms.CharField(widget=forms.TextInput, label="Type")
	serial_key = forms.CharField(widget=forms.TextInput, label="Serial Key")
	product_id = forms.CharField(widget=forms.TextInput, label="Product ID")
	installation_date = forms.DateField(required=False, widget=forms.DateInput(attrs={'type':'date'}), label="Installation Date")

	class Meta:
		model = OperatingSystem
		exclude = ['deployment_status', 'expiration_status', 'qrcode', 'expiration_date', 'is_archived']

	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)
		self.fields['os_name'].widget.attrs.update({'class': 'form-control'})
		self.fields['os_type'].widget.attrs.update({'class': 'form-control', 'maxlength': '5'})
		self.fields['product_id'].widget.attrs.update({'class': 'form-control'})
		self.fields['serial_key'].widget.attrs.update({'class': 'form-control'})
		self.fields['installation_date'].widget.attrs.update({'class': 'form-control'})

class MicrosoftOfficeForm(forms.ModelForm):
	mo_name = forms.CharField(widget=forms.TextInput, label="Name")
	mo_type = forms.CharField(widget=forms.TextInput, label="Type")
	serial_key = forms.CharField(widget=forms.TextInput, label="Serial Key")
	installation_date = forms.DateField(required=False, widget=forms.DateInput(attrs={'type':'date'}), label="Installation Date")

	class Meta:
		model = MicrosoftOffice
		exclude = ['deployment_status', 'expiration_status', 'qrcode', 'expiration_date', 'is_archived']

	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)
		self.fields['mo_name'].widget.attrs.update({'class': 'form-control'})
		self.fields['mo_type'].widget.attrs.update({'class': 'form-control', 'maxlength': '3'})
		self.fields['serial_key'].widget.attrs.update({'class': 'form-control'})
		self.fields['installation_date'].widget.attrs.update({'class': 'form-control'})

class OtherSoftwareForm(forms.ModelForm):
	software_name = forms.CharField(widget=forms.TextInput, label="Name")
	serial_key = forms.CharField(widget=forms.TextInput, label="Serial Key")
	installation_date = forms.DateField(widget=forms.DateInput(attrs={'type':'date'}), label="Installation Date")
	license_validity = forms.IntegerField(widget=forms.NumberInput, label="License Validity")

	class Meta:
		model = OtherSoftware
		exclude = ['deployment_status', 'expiration_status', 'qrcode', 'expiration_date', 'is_archived']

	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)
		self.fields['software_name'].widget.attrs.update({'class': 'form-control'})
		self.fields['serial_key'].widget.attrs.update({'class': 'form-control'})
		self.fields['installation_date'].widget.attrs.update({'class': 'form-control'})
		self.fields['license_validity'].widget.attrs.update({'class': 'form-control', 'min': '1', 'max': '10'})

class LocationForm(forms.ModelForm):
	building_name = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}), label="Building Name")
	office_name = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}), label="Office Name")
	category = forms.CharField(required=False, widget=forms.TextInput(attrs={'class': 'form-control'}), label="Category")
	center = forms.CharField(required=False, widget=forms.TextInput(attrs={'class': 'form-control'}), label="Center")

	class Meta:
		model = Location
		fields = ['building_name', 'office_name', 'category', 'center']

class MonitorForm(forms.ModelForm):
	brand = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}))
	model = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}))
	serial_number = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}))
	deployment_date = forms.DateField(required=False, widget=forms.DateInput(attrs={'type':'date', 'class': 'form-control'}), label="Deployment Date")
	life_span = forms.IntegerField(required=False, widget=forms.NumberInput, label="Life Span")

	class Meta:
		model = Monitor
		exclude = ['deployment_status', 'termination_status', 'qrcode', 'termination_date', 'is_archived']

	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)
		self.fields['life_span'].help_text = 'Enter the life span of this Monitor in YEARS. (e.g. 5)'
		self.fields['deployment_date'].widget.attrs.update({'class': 'form-control'})
		self.fields['life_span'].widget.attrs.update({'class': 'form-control', 'min': '1', 'max': '10'})

class PrinterForm(forms.ModelForm):
	brand = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}))
	model = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}))
	serial_number = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}))
	deployment_date = forms.DateField(required=False, widget=forms.DateInput(attrs={'type':'date'}), label="Deployment Date")
	life_span = forms.IntegerField(required=False, widget=forms.NumberInput, label="Life Span")

	class Meta:
		model = Printer
		exclude = ['deployment_status', 'termination_status', 'qrcode', 'termination_date', 'is_archived']

	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)
		self.fields['life_span'].help_text = 'Enter the life span of this Printer in YEARS. (e.g. 5)'
		self.fields['deployment_date'].widget.attrs.update({'class': 'form-control'})
		self.fields['life_span'].widget.attrs.update({'class': 'form-control', 'min': '1', 'max': '10'})

class ScannerForm(forms.ModelForm):
	brand = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}))
	model = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}))
	serial_number = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}))
	deployment_date = forms.DateField(required=False, widget=forms.DateInput(attrs={'type':'date', 'class': 'form-control'}), label="Deployment Date")
	life_span = forms.IntegerField(required=False, widget=forms.NumberInput, label="Life Span")

	class Meta:
		model = Scanner
		exclude = ['deployment_status', 'termination_status', 'qrcode', 'termination_date', 'is_archived']

	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)
		self.fields['life_span'].help_text = 'Enter the life span of this Scanner in YEARS. (e.g. 5)'
		self.fields['deployment_date'].widget.attrs.update({'class': 'form-control'})
		self.fields['life_span'].widget.attrs.update({'class': 'custom-select form-control', 'min': '1', 'max': '10'})

class TrackingFilterForm(forms.Form):
	qs = Location.objects.values_list('building_name', flat=True).distinct()
	option = forms.ModelChoiceField(empty_label="Choose building...", queryset=qs, widget=forms.Select, label="Building", required=False)

	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)
		self.fields['option'].widget.attrs.update({'class': 'custom-select', 'onchange': 'this.form.submit();'})
		self.fields['option'].choices = list(self.fields['option'].choices) + [('University', 'University')]

class UploadFileForm(forms.ModelForm):
	backup_file = forms.FileField(label="File")

	class Meta:
		model = Backup
		exclude = ['date_generated', 'file_size']
