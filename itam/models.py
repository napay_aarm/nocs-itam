from django.db import models
import datetime
from datetime import timedelta
from django.contrib.auth.models import (AbstractBaseUser, BaseUserManager)
from django.urls import reverse
from django.core.exceptions import ValidationError
from django.core.validators import RegexValidator
import re, string

mac_validator = RegexValidator(r"^([0-9a-f]{2}[:]){5}([0-9a-f]{2})$", "Please enter a valid MAC address.", flags=re.IGNORECASE)
ip_validator = RegexValidator(r"^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$", "Please enter a valid IP Address.")
nd_category_validator = RegexValidator(r"^[a-zA-Z]+$", "Please enter a valid network device category.")

class Backup(models.Model):
	backup_file = models.FileField(upload_to='backups/', blank=True, null=True)
	date_generated = models.DateTimeField(blank=True, null=True)
	file_size = models.PositiveIntegerField(blank=True, null=True)

	class Meta:
		ordering = ['-date_generated']

	def __str__(self):
		return str(self.backup_file)

class Location(models.Model):
	building_name = models.CharField(max_length=255)
	office_name = models.CharField(max_length=255)
	category = models.CharField(max_length=255, blank=True, null=True)
	center = models.CharField(max_length=255, blank=True, null=True)

	class Meta:
		ordering = ['building_name']
		unique_together = (('building_name', 'office_name'),)

	def __str__(self):
		return self.building_name + " - " + self.office_name

	def get_absolute_url(self):
		return reverse('itam:location-details', kwargs={'pk': self.pk})

	def natural_key(self):
		return(self.building_name + " - " + self.office_name)

class NetworkDevice(models.Model):
	STATUS = (('Deployed', 'Deployed'), ('Undeployed', 'Undeployed'))
	deployment_status = models.CharField(max_length=255, choices=STATUS, default="Undeployed")
	termination_status = models.BooleanField(default=False)
	category = models.CharField(max_length=255, validators=[nd_category_validator])
	code = models.CharField(max_length=255, unique=False)
	manufacturer = models.CharField(max_length=255, unique=False)
	details = models.CharField(max_length=255)
	primary_firmware = models.CharField(max_length=255)
	secondary_firmware = models.CharField(max_length=255)
	management_ip = models.CharField(max_length=15, unique=True, validators=[ip_validator])
	purchase_order = models.CharField(max_length=255)
	purchase_date = models.DateField()
	vendor = models.CharField(max_length=255)
	acquisition_price = models.DecimalField(max_digits=8, decimal_places=2)
	warranty_date = models.DateField()
	serial_number = models.CharField(max_length=255, unique=True)
	mac_address = models.CharField(max_length=17, unique=True, validators=[mac_validator])
	poe_status = models.CharField(null=True, blank=True, max_length=5)
	termination_date = models.DateField(blank=True, null=True)
	location = models.ForeignKey(Location, on_delete=models.SET_NULL, null=True, blank=True)
	deployment_date = models.DateField(blank=True, null=True)
	life_span = models.PositiveIntegerField(blank=True, null=True)
	is_archived = models.BooleanField(default=False)

	class Meta:
		ordering = ['-deployment_status']

	def __str__(self):
		return self.code + " (w/ " + str(self.sfp_set.count()) + " Sfp)" + " -- "  + self.deployment_status

	def get_absolute_url(self):
		return reverse('itam:network-device-details', kwargs={'pk': self.pk})

	def clean(self):
		self.code = self.code.upper()
		self.manufacturer = self.manufacturer.upper()
		self.mac_address = self.mac_address.upper()
		self.category = self.category.capitalize()
		self.category = self.category.strip()

	def natural_key(self):
		return(self.code + " (w/ " + str(self.sfp_set.count()) + " Sfp)")

class Sfp(models.Model):
	STATUS = (('Installed', 'Installed'), ('Not Installed', 'Not Installed'))
	deployment_status = models.CharField(max_length=255, choices=STATUS, default="Not Installed")
	termination_status = models.BooleanField(default=False)
	product_number = models.CharField(unique=False, max_length=255) #name
	manufacturer = models.CharField(max_length=255)
	serial_number = models.CharField(max_length=255, unique=True)
	product_description = models.CharField(max_length=255)
	purchase_order = models.CharField(max_length=255)
	vendor = models.CharField(max_length=255)
	purchase_date = models.DateField()
	termination_date = models.DateField(blank=True, null=True)
	location = models.ForeignKey(Location, on_delete=models.SET_NULL, null=True, blank=True)
	deployment_date = models.DateField(blank=True, null=True)
	life_span = models.PositiveIntegerField(blank=True, null=True)
	is_archived = models.BooleanField(default=False)
	network_device = models.ForeignKey(NetworkDevice, on_delete=models.CASCADE, null=True, blank=True)

	class Meta:
		ordering = ['-deployment_status']

	def __str__(self):
		return self.product_number

	def get_absolute_url(self):
		return reverse('itam:sfp-details', kwargs={'pk': self.pk})

	def clean(self):
		self.manufacturer = self.manufacturer.upper()
		self.product_number = self.product_number.upper()

	def natural_key(self):
		return(self.product_number)

class OperatingSystem(models.Model):
	os_name = models.CharField(max_length=255)
	os_type = models.CharField(max_length=5)
	product_id = models.CharField(max_length=255, blank=True, null=True, unique=True)
	serial_key = models.CharField(max_length=255, blank=True, null=True, unique=True)
	installation_date = models.DateField(blank=True, null=True,)
	STATUS = (('Not installed', 'Not Installed'), ('Installed', 'Installed'))
	deployment_status = models.CharField(max_length=255, choices=STATUS, default='Not installed')
	is_archived = models.BooleanField(default=False)

	class Meta:
		ordering = ['-deployment_status']

	def __str__(self):
		return self.os_name + " (" + self.deployment_status + ") "

	def clean(self):
		self.os_name = string.capwords(self.os_name)
		self.os_type = self.os_type.upper()

	def get_absolute_url(self):
		return reverse('itam:os-details', kwargs={'pk': self.pk})

	def natural_key(self):
		return(self.os_name)

class MicrosoftOffice(models.Model):
	mo_name = models.CharField(max_length=255)
	mo_type = models.CharField(max_length=3)
	serial_key = models.CharField(max_length=255, blank=True, null=True, unique=True)
	installation_date = models.DateField(blank=True, null=True,)
	STATUS = (('Not installed', 'Not Installed'), ('Installed', 'Installed'))
	deployment_status = models.CharField(max_length=255, choices=STATUS, default='Not installed')
	is_archived = models.BooleanField(default=False)

	class Meta:
		ordering = ['-deployment_status']

	def __str__(self):
		return self.mo_name + " (" + self.deployment_status + ") "

	def clean(self):
		self.mo_type = self.mo_type.upper()
		self.mo_name = string.capwords(self.mo_name)

	def get_absolute_url(self):
		return reverse('itam:mo-details', kwargs={'pk': self.pk})

	def natural_key(self):
		return(self.mo_name)

class OtherSoftware(models.Model):
	software_name = models.CharField(max_length=255)
	serial_key = models.CharField(max_length=255, blank=True, null=True, unique=True)
	installation_date = models.DateField()
	expiration_date = models.DateField(default=datetime.date.today)
	expiration_status = models.BooleanField(default=False)
	license_validity = models.PositiveIntegerField()
	STATUS = (('Not installed', 'Not Installed'), ('Installed', 'Installed'))
	deployment_status = models.CharField(max_length=255, choices=STATUS, default='Not installed')
	is_archived = models.BooleanField(default=False)

	class Meta:
		ordering = ['-deployment_status']

	def __str__(self):
		return self.software_name + " (" + self.deployment_status + ") "

	def get_absolute_url(self):
		return reverse('itam:other-details', kwargs={'pk': self.pk})

	def natural_key(self):
		return(self.software_name)

class Monitor(models.Model):
	STATUS = (('Deployed', 'Deployed'), ('Undeployed', 'Undeployed'))
	deployment_status = models.CharField(max_length=255, choices=STATUS, default="Undeployed")
	termination_status = models.BooleanField(default=False)
	brand = models.CharField(max_length=255)
	model = models.CharField(max_length=255)
	serial_number = models.CharField(max_length=255, unique=True)
	termination_date = models.DateField(blank=True, null=True)
	deployment_date = models.DateField(blank=True, null=True)
	life_span = models.PositiveIntegerField(blank=True, null=True)
	is_archived = models.BooleanField(default=False)

	class Meta:
		ordering = ['-deployment_status']

	def __str__(self):
		return self.brand + " - " + self.model + " (" + self.deployment_status + ") "

	def clean(self):
		self.brand = string.capwords(self.brand)
		self.model = string.capwords(self.model)

	def get_absolute_url(self):
		return reverse('itam:monitor-details', kwargs={'pk': self.pk})

	def natural_key(self): 
		return(self.brand + " - " + self.model) + " (" + self.deployment_status + ") "

class Printer(models.Model):
	STATUS = (('Deployed', 'Deployed'), ('Undeployed', 'Undeployed'))
	deployment_status = models.CharField(max_length=255, choices=STATUS, default="Undeployed")
	termination_status = models.BooleanField(default=False)
	brand = models.CharField(max_length=255)
	model = models.CharField(max_length=255)
	serial_number = models.CharField(max_length=255, unique=True)
	termination_date = models.DateField(blank=True, null=True)
	deployment_date = models.DateField(blank=True, null=True)
	life_span = models.PositiveIntegerField(blank=True, null=True)
	is_archived = models.BooleanField(default=False)

	class Meta:
		ordering = ['-deployment_status']

	def __str__(self):
		return self.model + " (" + self.deployment_status + ") "

	def clean(self):
		self.brand = string.capwords(self.brand)
		self.model = string.capwords(self.model)

	def get_absolute_url(self):
		return reverse('itam:printer-details', kwargs={'pk': self.pk})

	def natural_key(self):
		return(self.brand + " - " + self.model)

class Scanner(models.Model):
	STATUS = (('Deployed', 'Deployed'), ('Undeployed', 'Undeployed'))
	deployment_status = models.CharField(max_length=255, choices=STATUS, default="Undeployed")
	termination_status = models.BooleanField(default=False)
	brand = models.CharField(max_length=255)
	model = models.CharField(max_length=255)
	serial_number = models.CharField(max_length=255, unique=True)
	termination_date = models.DateField(blank=True, null=True)
	deployment_date = models.DateField(blank=True, null=True)
	life_span = models.PositiveIntegerField(blank=True, null=True)
	is_archived = models.BooleanField(default=False)

	class Meta:
		ordering = ['-deployment_status']

	def __str__(self):
		return self.model + " (" + self.deployment_status + ") " 

	def clean(self):
		self.brand = string.capwords(self.brand)
		self.model = string.capwords(self.model)

	def get_absolute_url(self):
		return reverse('itam:scanner-details', kwargs={'pk': self.pk})

	def natural_key(self):
		return(self.brand + " - " + self.model)

class Computer(models.Model):
	STATUS = (('Deployed', 'Deployed'), ('Undeployed', 'Undeployed'))
	deployment_status = models.CharField(max_length=255, choices=STATUS, default="Undeployed")
	termination_status = models.BooleanField(default=False)
	computer_id = models.CharField(unique=True, max_length=255)
	HTYPE = (('Desktop', 'Desktop'), ('Laptop', 'Laptop'))
	hardware_type = models.CharField(max_length=255, choices=HTYPE)
	system_specs = models.CharField(max_length=255)
	monitor = models.OneToOneField(Monitor, on_delete=models.SET_NULL, blank=True, null=True)
	printer = models.OneToOneField(Printer, on_delete=models.SET_NULL, blank=True, null=True)
	scanner = models.OneToOneField(Scanner, on_delete=models.SET_NULL, blank=True, null=True)
	base_license_os = models.CharField(max_length=255, blank=True, null=True)
	operating_system = models.OneToOneField(OperatingSystem, on_delete=models.SET_NULL, blank=True, null=True)
	microsoft_office = models.OneToOneField(MicrosoftOffice, on_delete=models.SET_NULL, blank=True, null=True)
	spss = models.CharField(max_length=255, blank=True, null=True)
	libre_office = models.CharField(max_length=255, blank=True, null=True)
	open_office = models.CharField(max_length=255, blank=True, null=True)
	other_software = models.OneToOneField(OtherSoftware, on_delete=models.SET_NULL, blank=True, null=True)
	termination_date = models.DateField(blank=True, null=True)
	deployment_date = models.DateField(blank=True, null=True)
	life_span = models.PositiveIntegerField(blank=True, null=True)
	location = models.ForeignKey(Location, on_delete=models.SET_NULL, blank=True, null=True)
	is_server = models.BooleanField(default=False)
	is_archived = models.BooleanField(default=False)

	class Meta:
		ordering = ['-deployment_status']

	def __str__(self):
		return self.computer_id

	def clean(self):
		self.computer_id = self.computer_id.upper()
		self.system_specs = string.capwords(self.system_specs)

	def get_absolute_url(self):
		return reverse('itam:computer-details', kwargs={'pk': self.pk})

	def natural_key(self):
		return(self.computer_id)

class UserManager(BaseUserManager):
	def create_user(self, username, email=None, password=None, first_name=None, last_name=None):
		user = self.model(
			username = username,
			email = self.normalize_email(email),
			first_name = first_name,
			last_name = last_name
		)
		user.set_password(password)
		user.save(using=self._db)
		return user

	def create_staff(self, username, email=None, password=None, first_name=None, last_name=None):
		user = self.create_user(
			username, 
			email=email,
			password=password, 
			first_name=first_name,
			last_name=last_name,
		)
		user.staff=True
		user.save(using=self._db)
		return user

	def create_superuser(self, username, email=None, password=None, first_name=None, last_name=None):
		user = self.create_user(
			username, 
			email=email,
			password=password, 
			first_name=first_name,
			last_name=last_name,
		)
		user.staff=True
		user.admin=True
		user.save(using=self._db)
		return user

class User(AbstractBaseUser):
	username = models.CharField(max_length=255, unique=True)
	email = models.EmailField(max_length=255)
	first_name = models.CharField(max_length=255)
	last_name = models.CharField(max_length=255)

	active = models.BooleanField(default=True)
	staff = models.BooleanField(default=False) # admin; can access django admin
	admin = models.BooleanField(default=False) # superuser

	USERNAME_FIELD = 'username'
	REQUIRED_FIELDS = ['first_name', 'last_name', 'email']

	objects = UserManager()

	def __str__(self):
		return self.username

	def get_full_name(self):
		return self.username + " " + self.first_name + " " + self.last_name

	def get_short_name(self):
		return self.username + " " + self.first_name + " " + self.last_name

	def has_perm(self, perm, obj=None):
		return True

	def has_module_perms(self, app_label):
		return True

	def get_absolute_url(self):
		return reverse('itam:user-details', kwargs={'pk': self.pk})

	@property
	def is_staff(self):
		return self.staff

	@property
	def is_admin(self):
		return self.admin

	@property
	def is_active(self):
		return self.active