from .models import *
from django.db.models.signals import post_save, pre_delete, post_delete, pre_save
from notifications.signals import notify
from django.contrib.auth.signals import user_logged_in
from django.dispatch import receiver
import datetime
from datetime import timedelta

@receiver(post_save, sender=Computer)
def computer_update_status(sender, instance, **kwargs):
	if instance.operating_system:
		OperatingSystem.objects.filter(id=instance.operating_system.id).update(deployment_status="Installed")
		
	for os in OperatingSystem.objects.all():
		if not hasattr(os, 'computer'):
			os.deployment_status = "Not Installed"
			os.save()

	if instance.other_software:
		OtherSoftware.objects.filter(id=instance.other_software.id).update(deployment_status="Installed")
	
	for s in OtherSoftware.objects.all():
		if not hasattr(s, 'computer'):
			s.deployment_status = "Not Installed"
			s.save()
	
	if instance.microsoft_office:
		MicrosoftOffice.objects.filter(id=instance.microsoft_office.id).update(deployment_status="Installed")
	
	for mo in MicrosoftOffice.objects.all():
		if not hasattr(mo, 'computer'):
			mo.deployment_status = "Not Installed"
			mo.save()

	if instance.location:
		if instance.monitor:
			Monitor.objects.filter(id=instance.monitor.id).update(deployment_status="Deployed")
		
		for x in Monitor.objects.all():
			if not hasattr(x, 'computer'):
				x.deployment_status = "Undeployed"
				x.save()

		if instance.printer:
			Printer.objects.filter(id=instance.printer.id).update(deployment_status="Deployed")
		
		for x in Printer.objects.all():
			if not hasattr(x, 'computer'):
				x.deployment_status = "Undeployed"
				x.save()

		if instance.scanner:
			Scanner.objects.filter(id=instance.scanner.id).update(deployment_status="Deployed")

		for x in Scanner.objects.all():
			if not hasattr(x, 'computer'):
				x.deployment_status = "Undeployed"
				x.save()

	if Location.objects.all().count() >= 1:
		if instance.location:
			Computer.objects.filter(id=instance.id).update(deployment_status="Deployed")
		else:
			Computer.objects.filter(id=instance.id).update(deployment_status="Undeployed")
	else: 
		Computer.objects.filter(id=instance.id).update(deployment_status="Undeployed")

	if instance.deployment_date:
		instance.termination_date = instance.deployment_date + (timedelta(days=366)*instance.life_span)

		if instance.termination_date <= datetime.date.today() + timedelta(days=366):
			Computer.objects.filter(id=instance.id).update(termination_status=True)
		else:
			Computer.objects.filter(id=instance.id).update(termination_status=False)

@receiver(post_delete, sender=Computer)
def computer_delete_update_software_status(sender, instance, **kwargs):
	for x in OperatingSystem.objects.all():
			if not hasattr(x, 'computer'):
				x.deployment_status = "Not Installed"
				x.save()

	for x in OtherSoftware.objects.all():
			if not hasattr(x, 'computer'):
				x.deployment_status = "Not Installed"
				x.save()

	for x in MicrosoftOffice.objects.all():
			if not hasattr(x, 'computer'):
				x.deployment_status = "Not Installed"
				x.save()

	for x in Monitor.objects.all():
			if not hasattr(x, 'computer'):
				x.deployment_status = "Undeployed"
				x.save()

	for x in Printer.objects.all():
			if not hasattr(x, 'computer'):
				x.deployment_status = "Undeployed"
				x.save()

	for x in Scanner.objects.all():
			if not hasattr(x, 'computer'):
				x.deployment_status = "Undeployed"
				x.save()

@receiver(post_save, sender=NetworkDevice)
def network_device_update_status(sender, instance, **kwargs):
	if Location.objects.all().count() >= 1:
		if not instance.location:
			NetworkDevice.objects.filter(id=instance.id).update(deployment_status="Undeployed")
		else:
			NetworkDevice.objects.filter(id=instance.id).update(deployment_status="Deployed")
	else:
		NetworkDevice.objects.filter(id=instance.id).update(deployment_status="Undeployed")

	if instance.deployment_date:
		instance.termination_date = instance.deployment_date + (timedelta(days=366)*instance.life_span)
		NetworkDevice.objects.filter(pk=instance.id).update(termination_date=instance.deployment_date + (timedelta(days=366)*instance.life_span))

		if instance.termination_date >= datetime.date.today() + timedelta(days=366):
			NetworkDevice.objects.filter(id=instance.id).update(termination_status=False)
		else:
			NetworkDevice.objects.filter(id=instance.id).update(termination_status=True)

	if instance.is_archived:
		instance.sfp_set.all().update(network_device=None, deployment_status="Not Installed")

@receiver(post_save, sender=Sfp)
def sfp_update_status(sender, instance, **kwargs):
	if Location.objects.all().count() >= 1:
		if not instance.network_device:
			Sfp.objects.filter(id=instance.id).update(deployment_status="Not Installed")
		else:
			Sfp.objects.filter(id=instance.id).update(deployment_status="Installed")
	else:
		Sfp.objects.filter(id=instance.id).update(deployment_status="Not Installed")

	if instance.deployment_date:
		instance.termination_date = instance.deployment_date + (timedelta(days=366)*instance.life_span)
		Sfp.objects.filter(pk=instance.id).update(termination_date=instance.deployment_date + (timedelta(days=366)*instance.life_span))

		if instance.termination_date >= datetime.date.today() + timedelta(days=366):
			Sfp.objects.filter(id=instance.id).update(termination_status=False)
		else:
			Sfp.objects.filter(id=instance.id).update(termination_status=True)

@receiver(post_save, sender=Monitor)
def monitor_update_status(sender, instance, **kwargs):
	if Monitor.objects.all().count() >= 1:
		if hasattr(instance, 'computer'):
			if instance.computer.location:
				Monitor.objects.filter(pk=instance.id).update(deployment_status="Deployed")
			else:
				Monitor.objects.filter(pk=instance.id).update(deployment_status="Undeployed")
	else:
		Monitor.objects.all().update(deployment_status="Undeployed")

	if instance.deployment_date:
		instance.termination_date = instance.deployment_date + (timedelta(days=366)*instance.life_span)
		Monitor.objects.filter(pk=instance.id).update(termination_date=instance.deployment_date + (timedelta(days=366)*instance.life_span))

		if instance.termination_date >= datetime.date.today() + timedelta(days=366):
			Monitor.objects.filter(id=instance.id).update(termination_status=False)
		else:
			Monitor.objects.filter(id=instance.id).update(termination_status=True)

@receiver(post_save, sender=Printer)
def printer_update_status(sender, instance, **kwargs):
	if Printer.objects.all().count() >= 1:
		if hasattr(instance, 'computer'):
			if instance.computer.location:
				Printer.objects.filter(pk=instance.id).update(deployment_status="Deployed")
			else:
				Printer.objects.filter(pk=instance.id).update(deployment_status="Undeployed")
	else:
		Printer.objects.all().update(deployment_status="Undeployed")

	if instance.deployment_date:
		instance.termination_date = instance.deployment_date + (timedelta(days=366)*instance.life_span)
		Printer.objects.filter(pk=instance.id).update(termination_date=instance.deployment_date + (timedelta(days=366)*instance.life_span))

		if instance.termination_date >= datetime.date.today() + timedelta(days=366):
			Printer.objects.filter(id=instance.id).update(termination_status=False)
		else:
			Printer.objects.filter(id=instance.id).update(termination_status=True)

@receiver(post_save, sender=Scanner)
def scanner_update_status(sender, instance, **kwargs):
	if Scanner.objects.all().count() >= 1:
		if hasattr(instance, 'computer'):
			if instance.computer.location:
				Scanner.objects.filter(pk=instance.id).update(deployment_status="Deployed")
			else:
				Scanner.objects.filter(pk=instance.id).update(deployment_status="Undeployed")
	else:
		Scanner.objects.all().update(deployment_status="Undeployed")

	if instance.deployment_date:
		instance.termination_date = instance.deployment_date + (timedelta(days=366)*instance.life_span)
		Scanner.objects.filter(pk=instance.id).update(termination_date=instance.deployment_date + (timedelta(days=366)*instance.life_span))

		if instance.termination_date >= datetime.date.today() + timedelta(days=366):
			Scanner.objects.filter(id=instance.id).update(termination_status=False)
		else:
			Scanner.objects.filter(id=instance.id).update(termination_status=True)

@receiver(post_save, sender=OperatingSystem)
def os_update_status(sender, instance, **kwargs):
	if Computer.objects.all().count() >= 1:
		if hasattr(instance, 'computer'):
			OperatingSystem.objects.filter(pk=instance.id).update(deployment_status="Installed")
		else:
			OperatingSystem.objects.filter(pk=instance.id).update(deployment_status="Not Installed")
			instance.installation_date = None
	else:
		OperatingSystem.objects.all().update(deployment_status="Not Installed")

	if not instance.installation_date:
		OperatingSystem.objects.filter(pk=instance.id).update(deployment_status="Not Installed")
	else:
		OperatingSystem.objects.filter(pk=instance.id).update(deployment_status="Installed")

@receiver(post_save, sender=MicrosoftOffice)
def mo_update_status(sender, instance, **kwargs):
	if Computer.objects.all().count() >= 1:
		if hasattr(instance, 'computer'):
			MicrosoftOffice.objects.filter(pk=instance.id).update(deployment_status="Installed")
		else:
			MicrosoftOffice.objects.filter(pk=instance.id).update(deployment_status="Not Installed")
			instance.installation_date = None
	else:
		MicrosoftOffice.objects.all().update(deployment_status="Not Installed")

	if not instance.installation_date:
		MicrosoftOffice.objects.filter(pk=instance.id).update(deployment_status="Not Installed")
	else:
		MicrosoftOffice.objects.filter(pk=instance.id).update(deployment_status="Installed")

@receiver(post_save, sender=OtherSoftware)
def software_update_status(sender, instance, **kwargs):
	if Computer.objects.all().count() >= 1:
		if hasattr(instance, 'computer'):
			OtherSoftware.objects.filter(pk=instance.id).update(deployment_status="Installed")
		else:
			OtherSoftware.objects.filter(pk=instance.id).update(deployment_status="Not Installed")
			instance.installation_date = None
	else:
		OtherSoftware.objects.all().update(deployment_status="Not Installed")

	if instance.installation_date:
		instance.expiration_date = instance.installation_date + (timedelta(days=366)*instance.license_validity)
		OtherSoftware.objects.filter(pk=instance.id).update(expiration_date=instance.installation_date + (timedelta(days=366)*instance.license_validity))
		
		if instance.expiration_date >= datetime.date.today() + timedelta(days=366):
			OtherSoftware.objects.filter(id=instance.id).update(expiration_status=False)
		else:
			OtherSoftware.objects.filter(id=instance.id).update(expiration_status=True)

@receiver(user_logged_in)
def notify_user(sender, request, user, **kwargs):
	"""
	Sends a notification about expiring or to be terminated assets to the user who logged in
	"""
	computer_objects = Computer.objects.filter(is_archived=False, location__isnull=False)
	printer_objects = Printer.objects.filter(is_archived=False, computer__location__isnull=False)
	monitor_objects = Monitor.objects.filter(is_archived=False, computer__location__isnull=False)
	scanner_objects = Scanner.objects.filter(is_archived=False, computer__location__isnull=False)
	network_device_objects = NetworkDevice.objects.filter(is_archived=False, location__isnull=False)
	sfp_objects = Sfp.objects.filter(is_archived=False, network_device__location__isnull=False)
	other_objects = OtherSoftware.objects.filter(is_archived=False, computer__location__isnull=False)
	notification_date = datetime.date.today() + timedelta(days=366)

	for c in computer_objects:
		if c.termination_date:
			if c.termination_date <= notification_date: 
				c.termination_status = True

			if c.termination_status:
				notify.send(c, recipient=user, verb="will be terminated on %s" % c.termination_date)

	for p in printer_objects:
		if p.termination_date:
			if p.termination_date <= notification_date:
				p.termination_status = True

			if p.termination_status:
				notify.send(p, recipient=user, verb="will be terminated on %s" % p.termination_date)

	for m in monitor_objects:
		if m.termination_date:
			if m.termination_date <= notification_date:
				m.termination_status = True

			if m.termination_status:
				notify.send(m, recipient=user, verb="will be terminated on %s" % m.termination_date)

	for scanner in scanner_objects:
		if scanner.termination_date:
			if scanner.termination_date <= notification_date:
				scanner.termination_status = True

			if scanner.termination_status:
				notify.send(scanner, recipient=user, verb="will be terminated on %s" % scanner.termination_date)

	for nd in network_device_objects:
		if nd.termination_date:
			if nd.termination_date <= notification_date:
				nd.termination_status = True

			if nd.termination_status:
				notify.send(nd, recipient=user, verb="will be terminated on %s" % nd.termination_date)

	for sfp in sfp_objects:
		if sfp.termination_date:
			if sfp.termination_date <= notification_date:
				sfp.termination_status = True

			if sfp.termination_status:
				notify.send(sfp, recipient=user, verb="will be terminated on %s" % sfp.termination_date)

	for other in other_objects:
		if other.expiration_date <= notification_date:
			other.expiration_status = True

		if other.expiration_status:
			notify.send(other, recipient=user, verb="will expire next year %s" % other.expiration_date)

@receiver(post_delete, sender=Location)
def on_location_delete(sender, instance, **kwargs):
	for obj in Computer.objects.all():
		if not obj.location:
			obj.deployment_status = "Undeployed"
			obj.save()
			if obj.monitor:
				Monitor.objects.filter(pk=obj.monitor.id).update(deployment_status="Undeployed")
			if obj.scanner:
				Scanner.objects.filter(pk=obj.scanner.id).update(deployment_status="Undeployed")
			if obj.printer:
				Printer.objects.filter(pk=obj.printer.id).update(deployment_status="Undeployed")

	for obj in NetworkDevice.objects.all():
		if not obj.location:
			obj.deployment_status = "Undeployed"
			obj.save()
