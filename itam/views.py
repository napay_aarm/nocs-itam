from django.http import HttpResponseRedirect, HttpResponse, Http404
from django.conf import settings
from django.shortcuts import render, redirect
from django.urls import reverse_lazy, reverse
from .forms import *
from django.contrib.auth import (authenticate, login, logout)
from django.contrib.auth.forms import PasswordChangeForm
from django.views.generic import TemplateView, ListView, DetailView, View
from django.views.generic.edit import CreateView, DeleteView, UpdateView, FormView
from .models import *
from io import BytesIO, StringIO
from django.core.files import File
from django.db.models import Count
from django.views.generic.edit import FormMixin
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import user_passes_test
from django.contrib.auth.forms import PasswordChangeForm
from django.contrib import messages
from actstream import action
from actstream.models import Action
from notifications.models import Notification 
from notifications.signals import notify
from django.core import serializers
import json, base64, pyqrcode
from django.utils import timezone
from .render import Render
from django.db.models import Q
from django.core.management import call_command
import datetime


def login_view(request):
	form = 	UserLoginForm(request.POST or None)
	if form.is_valid():
		username = form.cleaned_data.get("username")
		password = form.cleaned_data.get("password")
		user = authenticate(username=username, password=password)
		login(request, user)
		messages.success(request, "You have successfully logged in.")
		return redirect('itam:index')
	if request.user.is_authenticated:
		return redirect('itam:index')
	else:
		return render(request, "itam/login.html", {"form" : form})
	
def logout_view(request):
	messages.success(request, "You have successfully logged out.")
	logout(request)
	return HttpResponseRedirect(settings.LOGIN_URL)

def change_password(request):
	if request.method == 'POST':
		form = PasswordChangeForm(request.user, request.POST)
		if form.is_valid():
			user = form.save()
			update_session_auth_hash(request, user)
			messages.success(request, "Your password was successfully updated.")
			return redirect('itam:change-password')
		else:
			messages.error(request, 'Please check for errors below.')
	else:
		form = PasswordChangeForm(request.user)
	return render(request, "itam/user/change-password.html", {"form": form})

class IndexView(TemplateView):
	template_name = "itam/index.html"

	def get_context_data(self, **kwargs):
		context = super(IndexView, self).get_context_data(**kwargs)
		context['active_page'] = 'home'
		context['desktop_count'] = (Computer.objects.count() + Printer.objects.count() + Scanner.objects.count() + Monitor.objects.count())
		context['network_count'] = NetworkDevice.objects.count() + Sfp.objects.count()
		context['software_count'] = OperatingSystem.objects.count() + MicrosoftOffice.objects.count() + OtherSoftware.objects.count()
		context['location_count'] = Location.objects.count()
		context['user_count'] = User.objects.count()
		return context

class ActivityLogView(ListView):
	template_name = "itam/activity_log.html"
	model = Action
	paginate_by = 10
	context_object_name = "actions"

	@method_decorator(user_passes_test(lambda user: user.is_admin))
	def dispatch(self, *args, **kwargs):
		return super().dispatch(*args, **kwargs)

	def get_queryset(self):
		filter_input = self.request.GET.get("filterInput")
		if self.request.GET:
			if filter_input:
				if filter_input == "All":
					return Action.objects.all()
				else:	
					return Action.objects.filter(Q(verb__search=filter_input))
		return Action.objects.all()

	def get_paginate_by(self, queryset):
		return self.request.GET.get('paginate_by', self.paginate_by)

	def get_context_data(self, **kwargs):
		context = super(ActivityLogView, self).get_context_data(**kwargs)
		context['filter_value'] = self.request.GET.get("filterInput")
		context['paginate_value'] = self.request.GET.get('paginate_by', self.paginate_by)
		return context

class NotificationView(TemplateView):
	template_name = "itam/notifications.html"

	def get_context_data(self, **kwargs):
		context = super(NotificationView, self).get_context_data(**kwargs)
		context['unread_notifs'] = Notification.objects.filter(recipient=self.request.user, unread=True) 
		context['read_notifs'] = Notification.objects.filter(recipient=self.request.user, unread=False)
		return context

def mark_all_as_read_notification(request):
	Notification.objects.mark_all_as_read(recipient=request.user)
	return redirect('itam:notification')

def mark_as_read_notification(request, pk):
	selected_notification = Notification.objects.get(pk=request.POST['a.id'])
	selected_notification.mark_as_read()
	return redirect('itam:notification')

def delete_all_notification(request):
	Notification.objects.filter(recipient=request.user, unread=False).delete()
	return redirect('itam:notification')

def delete_notification(request, pk):
	selected_notification = Notification.objects.get(pk=request.POST['a.id'])
	selected_notification.delete()
	return redirect('itam:notification')

class ComputerList(ListView):
	template_name = "itam/hardware/computer/display-computers.html"
	model = Computer
	context_object_name = "computers"
	paginate_by = 10

	def get_context_data(self, **kwargs):
		context = super(ComputerList, self).get_context_data(**kwargs)
		context['active_page'] = 'computer'
		context['filter_value'] = self.request.GET.get("filterInput")
		context['paginate_value'] = self.request.GET.get('paginate_by', self.paginate_by)
		return context

	def get_queryset(self):
		filter_input = self.request.GET.get("filterInput")
		search_input = self.request.GET.get("searchInput") 
		if self.request.GET:
			if filter_input:
				if filter_input == "All":
					return Computer.objects.filter(is_archived=False)
				else:	
					return Computer.objects.filter(Q(is_archived=False, deployment_status__search=filter_input))
			elif search_input:
				return Computer.objects.filter(Q(is_archived=False, computer_id__icontains=search_input) | 
					Q(is_archived=False, hardware_type__icontains=search_input) | 
					Q(is_archived=False, deployment_status__icontains=search_input) |
					Q(is_archived=False, termination_date__icontains=search_input))
		return Computer.objects.filter(is_archived=False)

	def get_paginate_by(self, queryset):
		return self.request.GET.get('paginate_by', self.paginate_by)

class ComputerDetails(DetailView):
	template_name = "itam/hardware/computer/computer-details.html"
	model = Computer

	def get_context_data(self, **kwargs):
		context = super(ComputerDetails, self).get_context_data(**kwargs)
		context['active_page'] = 'computer'
		return context

class ComputerCreate(CreateView):
	template_name = "itam/hardware/computer/add-computer.html"
	model = Computer
	context_object_name = "computer"
	form_class = ComputerForm
	success_url = reverse_lazy('itam:computer-list')

	def get_context_data(self, **kwargs):
		context = super(ComputerCreate, self).get_context_data(**kwargs)
		context['active_page'] = 'computer'
		return context

	def form_valid(self, form):
		self.object = form.save()
		action.send(self.request.user, verb='created', action_object=form.instance)
		messages.success(self.request, "Asset created successfully.")
		return super().form_valid(form)

class ComputerUpdate(UpdateView):
	template_name = "itam/hardware/computer/edit-computer.html"
	model = Computer
	form_class = ComputerForm

	def get_context_data(self, **kwargs):
		context = super(ComputerUpdate, self).get_context_data(**kwargs)
		context['active_page'] = 'computer'
		return context

	def form_valid(self, form):
		self.object = form.save()
		action.send(self.request.user, verb='updated', action_object=form.instance)
		messages.success(self.request, "Asset updated successfully.")
		return super().form_valid(form)

class ComputerDelete(DeleteView):
	model = Computer
	success_url = reverse_lazy('itam:computer-list')

	def delete(self, request, *args, **kwargs):
		self.object = self.get_object()
		action.send(self.request.user, verb='deleted %s' % self.object)
		messages.success(self.request, "Asset deleted successfully.")
		self.object.delete()
		return HttpResponseRedirect(self.get_success_url())

class SfpList(ListView):
	template_name = "itam/hardware/sfp/display-sfps.html"
	model = Sfp
	context_object_name = "sfps"
	paginate_by = 10

	def get_context_data(self, **kwargs):
		context = super(SfpList, self).get_context_data(**kwargs)
		context['active_page'] = 'sfp'
		context['paginate_value'] = self.request.GET.get('paginate_by', self.paginate_by)
		context['filter_value'] = self.request.GET.get("filterInput")
		return context

	def get_queryset(self):
		filter_input = self.request.GET.get("filterInput")
		search_input = self.request.GET.get("searchInput") 
		if self.request.GET:
			if filter_input:
				if filter_input == "All":
					return Sfp.objects.filter(is_archived=False)
				else:
					return Sfp.objects.filter(Q(is_archived=False, deployment_status__exact=filter_input))
			elif search_input:
				return Sfp.objects.filter(Q(is_archived=False, product_number__icontains=search_input) | 
					Q(is_archived=False, manufacturer__icontains=search_input) | 
					Q(is_archived=False, deployment_status__icontains=search_input) |
					Q(is_archived=False, termination_date__icontains=search_input) |
					Q(is_archived=False, network_device__code__icontains=search_input))
		return Sfp.objects.filter(is_archived=False)

	def get_paginate_by(self, queryset):
		return self.request.GET.get('paginate_by', self.paginate_by)

class SfpDetails(DetailView):
	template_name = "itam/hardware/sfp/sfp-details.html"
	model = Sfp

	def get_context_data(self, **kwargs):
		context = super(SfpDetails, self).get_context_data(**kwargs)
		context['active_page'] = 'sfp'
		return context

class SfpCreate(CreateView):
	template_name = "itam/hardware/sfp/add-sfp.html"
	model = Sfp
	context_object_name = "sfp"
	form_class = SfpForm
	success_url = reverse_lazy('itam:sfp-list')

	def get_context_data(self, **kwargs):
		context = super(SfpCreate, self).get_context_data(**kwargs)
		context['active_page'] = 'sfp'
		return context

	def form_valid(self, form):
		self.object = form.save()
		action.send(self.request.user, verb='created', action_object=form.instance)
		messages.success(self.request, "Asset created successfully.")
		return super().form_valid(form)

class SfpUpdate(UpdateView):
	template_name = "itam/hardware/sfp/edit-sfp.html"
	model = Sfp
	form_class = SfpForm

	def get_context_data(self, **kwargs):
		context = super(SfpUpdate, self).get_context_data(**kwargs)
		context['active_page'] = 'sfp'
		return context

	def form_valid(self, form):
		self.object = form.save()
		action.send(self.request.user, verb='updated', action_object=form.instance)
		messages.success(self.request, "Asset updated successfully.")
		return super().form_valid(form)

class SfpDelete(DeleteView):
	model = Sfp
	success_url = reverse_lazy('itam:sfp-list')

	def delete(self, request, *args, **kwargs):
		self.object = self.get_object()
		action.send(self.request.user, verb='deleted %s' % self.object)
		messages.success(self.request, "Asset deleted successfully.")
		self.object.delete()
		return HttpResponseRedirect(self.get_success_url())

class NetworkDeviceList(ListView):
	"""
	Displays a list of Unarchived Network Devices
	"""
	template_name = "itam/hardware/network_device/display-network-devices.html"
	model = NetworkDevice
	context_object_name = "network_devices"
	paginate_by = 10

	def get_context_data(self, **kwargs):
		context = super(NetworkDeviceList, self).get_context_data(**kwargs)
		context['active_page'] = 'network_device'
		context['filter_value'] = self.request.GET.get("filterInput")
		context['paginate_value'] = self.request.GET.get('paginate_by', self.paginate_by)
		return context

	def get_queryset(self):
		filter_input = self.request.GET.get("filterInput")
		search_input = self.request.GET.get("searchInput") 
		if self.request.GET:
			if filter_input:
				if filter_input == "All":
					return NetworkDevice.objects.filter(is_archived=False)
				else:	
					return NetworkDevice.objects.filter(Q(is_archived=False, deployment_status__search=filter_input))
			elif search_input:
				return NetworkDevice.objects.filter(Q(is_archived=False, code__icontains=search_input) | 
					Q(is_archived=False, category__icontains=search_input) | 
					Q(is_archived=False, deployment_status__icontains=search_input) |
					Q(is_archived=False, termination_date__icontains=search_input) |
					Q(is_archived=False, location__building_name__icontains=search_input)|
					Q(is_archived=False, location__office_name__icontains=search_input))
		return NetworkDevice.objects.filter(is_archived=False)

	def get_paginate_by(self, queryset):
		return self.request.GET.get('paginate_by', self.paginate_by)

class NetworkDeviceDetails(DetailView):
	template_name = "itam/hardware/network_device/network-device-details.html"
	model = NetworkDevice

	def get_context_data(self, **kwargs):
		context = super(NetworkDeviceDetails, self).get_context_data(**kwargs)
		context['active_page'] = 'network_device'
		return context

class NetworkDeviceCreate(CreateView):
	template_name = "itam/hardware/network_device/add-network-device.html"
	model = NetworkDevice
	form_class = NetworkDeviceForm
	success_url = reverse_lazy('itam:network-device-list')

	def get_context_data(self, **kwargs):
		context = super(NetworkDeviceCreate, self).get_context_data(**kwargs)
		context['active_page'] = 'network_device'
		return context

	def form_valid(self, form):
		self.object = form.save()
		messages.success(self.request, "Asset created successfully.")
		action.send(self.request.user, verb='created', action_object=form.instance)
		return super().form_valid(form)

class NetworkDeviceUpdate(UpdateView):
	template_name = "itam/hardware/network_device/edit-network-device.html"
	model = NetworkDevice
	form_class = NetworkDeviceForm

	def get_context_data(self, **kwargs):
		context = super(NetworkDeviceUpdate, self).get_context_data(**kwargs)
		context['active_page'] = 'network_device'
		return context

	def form_valid(self, form):
		self.object = form.save()
		messages.success(self.request, "Asset updated successfully.")
		action.send(self.request.user, verb='updated', action_object=form.instance)
		return super().form_valid(form)

class NetworkDeviceDelete(DeleteView):
	model = NetworkDevice
	success_url = reverse_lazy('itam:network-device-list')

	def delete(self, request, *args, **kwargs):
		self.object = self.get_object()
		messages.success(self.request, "Asset deleted successfully.")
		action.send(self.request.user, verb='deleted %s' % self.object)
		self.object.delete()
		return HttpResponseRedirect(self.get_success_url())

##### MONITOR
class MonitorList(ListView):
	template_name = "itam/hardware/monitor/display-monitors.html"
	model = Monitor
	context_object_name = "monitors"
	paginate_by = 10

	def get_context_data(self, **kwargs):
		context = super(MonitorList, self).get_context_data(**kwargs)
		context['active_page'] = 'monitor'
		context['paginate_value'] = self.request.GET.get('paginate_by', self.paginate_by)
		context['filter_value'] = self.request.GET.get("filterInput")
		return context

	def get_queryset(self):
		filter_input = self.request.GET.get("filterInput")
		search_input = self.request.GET.get("searchInput") 
		if self.request.GET:
			if filter_input:
				if filter_input == "All":
					return Monitor.objects.filter(is_archived=False)
				else:
					return Monitor.objects.filter(Q(is_archived=False, deployment_status__search=filter_input))
			elif search_input:
				return Monitor.objects.filter(Q(is_archived=False, model__icontains=search_input) | 
					Q(is_archived=False, brand__icontains=search_input) | 
					Q(is_archived=False, deployment_status__icontains=search_input) |
					Q(is_archived=False, termination_date__icontains=search_input) |
					Q(is_archived=False, computer__computer_id__icontains=search_input))
		return Monitor.objects.filter(is_archived=False)

	def get_paginate_by(self, queryset):
		return self.request.GET.get('paginate_by', self.paginate_by)

class MonitorDetails(DetailView):
	template_name = "itam/hardware/monitor/monitor-details.html"
	model = Monitor

	def get_context_data(self, **kwargs):
		context = super(MonitorDetails, self).get_context_data(**kwargs)
		context['active_page'] = 'monitor'
		return context

class MonitorCreate(CreateView):
	template_name = "itam/hardware/monitor/add-monitor.html"
	model = Monitor
	form_class = MonitorForm
	success_url = reverse_lazy('itam:monitor-list')

	def get_context_data(self, **kwargs):
		context = super(MonitorCreate, self).get_context_data(**kwargs)
		context['active_page'] = 'monitor'
		return context

	def form_valid(self, form):
		self.object = form.save()
		messages.success(self.request, "Asset created successfully.")
		action.send(self.request.user, verb='created', action_object=form.instance)
		return super().form_valid(form)

class MonitorUpdate(UpdateView):
	template_name = "itam/hardware/monitor/edit-monitor.html"
	model = Monitor
	form_class = MonitorForm

	def get_context_data(self, **kwargs):
		context = super(MonitorUpdate, self).get_context_data(**kwargs)
		context['active_page'] = 'monitor'
		return context

	def form_valid(self, form):
		self.object = form.save()
		messages.success(self.request, "Asset updated successfully.")
		action.send(self.request.user, verb='updated', action_object=form.instance)
		return super().form_valid(form)

class MonitorDelete(DeleteView):
	model = Monitor
	success_url = reverse_lazy('itam:monitor-list')

	def delete(self, request, *args, **kwargs):
		self.object = self.get_object()
		messages.success(self.request, "Asset deleted successfully.")
		action.send(self.request.user, verb='deleted %s' % self.object)
		self.object.delete()
		return HttpResponseRedirect(self.get_success_url())
### END OF MONITOR
### PRINTER
class PrinterList(ListView):
	template_name = "itam/hardware/printer/display-printers.html"
	model = Printer
	context_object_name = "printers"
	paginate_by = 10

	def get_context_data(self, **kwargs):
		context = super(PrinterList, self).get_context_data(**kwargs)
		context['active_page'] = 'printer'
		context['paginate_value'] = self.request.GET.get('paginate_by', self.paginate_by)
		context['filter_value'] = self.request.GET.get("filterInput")
		return context

	def get_queryset(self):
		filter_input = self.request.GET.get("filterInput")
		search_input = self.request.GET.get("searchInput") 
		if self.request.GET:
			if filter_input:
				if filter_input == "All":
					return Printer.objects.filter(is_archived=False)
				else:
					return Printer.objects.filter(Q(is_archived=False, deployment_status__search=filter_input))
			elif search_input:
				return Printer.objects.filter(Q(is_archived=False, model__icontains=search_input) | 
					Q(is_archived=False, brand__icontains=search_input) | 
					Q(is_archived=False, deployment_status__icontains=search_input) |
					Q(is_archived=False, termination_date__icontains=search_input) |
					Q(is_archived=False, computer__computer_id__icontains=search_input))
		return Printer.objects.filter(is_archived=False)

	def get_paginate_by(self, queryset):
		return self.request.GET.get('paginate_by', self.paginate_by)

class PrinterDetails(DetailView):
	template_name = "itam/hardware/printer/printer-details.html"
	model = Printer

	def get_context_data(self, **kwargs):
		context = super(PrinterDetails, self).get_context_data(**kwargs)
		context['active_page'] = 'printer'
		return context

class PrinterCreate(CreateView):
	template_name = "itam/hardware/printer/add-printer.html"
	model = Printer
	form_class = PrinterForm
	success_url = reverse_lazy('itam:printer-list')

	def get_context_data(self, **kwargs):
		context = super(PrinterCreate, self).get_context_data(**kwargs)
		context['active_page'] = 'printer'
		return context

	def form_valid(self, form):
		self.object = form.save()
		messages.success(self.request, "Asset created successfully.")
		action.send(self.request.user, verb='created', action_object=form.instance)
		return super().form_valid(form)

class PrinterUpdate(UpdateView):
	template_name = "itam/hardware/printer/edit-printer.html"
	model = Printer
	form_class = PrinterForm

	def get_context_data(self, **kwargs):
		context = super(PrinterUpdate, self).get_context_data(**kwargs)
		context['active_page'] = 'printer'
		return context

	def form_valid(self, form):
		self.object = form.save()
		messages.success(self.request, "Asset updated successfully.")
		action.send(self.request.user, verb='updated', action_object=form.instance)
		return super().form_valid(form)

class PrinterDelete(DeleteView):
	model = Printer
	success_url = reverse_lazy('itam:printer-list')

	def delete(self, request, *args, **kwargs):
		self.object = self.get_object()
		messages.success(self.request, "Asset deleted successfully.")
		action.send(self.request.user, verb='deleted %s' % self.object)
		self.object.delete()
		return HttpResponseRedirect(self.get_success_url())
###END OF PRINTER
### SCANNER
class ScannerList(ListView):
	template_name = "itam/hardware/scanner/display-scanners.html"
	model = Scanner
	context_object_name = "scanners"
	paginate_by = 10

	def get_context_data(self, **kwargs):
		context = super(ScannerList, self).get_context_data(**kwargs)
		context['active_page'] = 'scanner'
		context['paginate_value'] = self.request.GET.get('paginate_by', self.paginate_by)
		context['filter_value'] = self.request.GET.get("filterInput")
		return context

	def get_queryset(self):
		filter_input = self.request.GET.get("filterInput")
		search_input = self.request.GET.get("searchInput") 
		if self.request.GET:
			if filter_input:
				if filter_input == "All":
					return Scanner.objects.filter(is_archived=False)
				else:
					return Scanner.objects.filter(Q(is_archived=False, deployment_status__search=filter_input))
			elif search_input:
				return Scanner.objects.filter(Q(is_archived=False, model__icontains=search_input) | 
					Q(is_archived=False, brand__icontains=search_input) | 
					Q(is_archived=False, deployment_status__icontains=search_input) |
					Q(is_archived=False, termination_date__icontains=search_input) |
					Q(is_archived=False, computer__computer_id__icontains=search_input))
		return Scanner.objects.filter(is_archived=False)

	def get_paginate_by(self, queryset):
		return self.request.GET.get('paginate_by', self.paginate_by)

class ScannerDetails(DetailView):
	template_name = "itam/hardware/scanner/scanner-details.html"
	model = Scanner

	def get_context_data(self, **kwargs):
		context = super(ScannerDetails, self).get_context_data(**kwargs)
		context['active_page'] = 'scanner'
		return context

class ScannerCreate(CreateView):
	template_name = "itam/hardware/scanner/add-scanner.html"
	model = Scanner
	form_class = ScannerForm
	success_url = reverse_lazy('itam:scanner-list')

	def get_context_data(self, **kwargs):
		context = super(ScannerCreate, self).get_context_data(**kwargs)
		context['active_page'] = 'scanner'
		return context

	def form_valid(self, form):
		self.object = form.save()
		messages.success(self.request, "Asset created successfully.")
		action.send(self.request.user, verb='created', action_object=form.instance)
		return super().form_valid(form)

class ScannerUpdate(UpdateView):
	template_name = "itam/hardware/scanner/edit-scanner.html"
	model = Scanner
	form_class = ScannerForm

	def get_context_data(self, **kwargs):
		context = super(ScannerUpdate, self).get_context_data(**kwargs)
		context['active_page'] = 'scanner'
		return context

	def form_valid(self, form):
		self.object = form.save()
		messages.success(self.request, "Asset updated successfully.")
		action.send(self.request.user, verb='updated', action_object=form.instance)
		return super().form_valid(form)

class ScannerDelete(DeleteView):
	model = Scanner
	success_url = reverse_lazy('itam:scanner-list')

	def delete(self, request, *args, **kwargs):
		self.object = self.get_object()
		messages.success(self.request, "Asset deleted successfully.")
		action.send(self.request.user, verb='deleted %s' % self.object)
		self.object.delete()
		return HttpResponseRedirect(self.get_success_url())
###END SCANNER

class OperatingSystemList(ListView):
	template_name = "itam/software/os/display-os.html"
	model = OperatingSystem
	context_object_name = "os_objects"
	paginate_by = 10

	def get_context_data(self, **kwargs):
		context = super(OperatingSystemList, self).get_context_data(**kwargs)
		context['active_page'] = 'os'
		context['paginate_value'] = self.request.GET.get('paginate_by', self.paginate_by)
		context['filter_value'] = self.request.GET.get("filterInput")
		return context

	def get_queryset(self):
		filter_input = self.request.GET.get("filterInput")
		search_input = self.request.GET.get("searchInput") 
		if self.request.GET:
			if filter_input:
				if filter_input == "All":
					return OperatingSystem.objects.filter(is_archived=False)
				else:
					return OperatingSystem.objects.filter(Q(is_archived=False, deployment_status__exact=filter_input))
			elif search_input:
				return OperatingSystem.objects.filter(Q(is_archived=False, os_name__icontains=search_input) | 
					Q(is_archived=False, os_type__icontains=search_input) | 
					Q(is_archived=False, deployment_status__icontains=search_input) |
					Q(is_archived=False, computer__computer_id__icontains=search_input))
		return OperatingSystem.objects.filter(is_archived=False)

	def get_paginate_by(self, queryset):
		return self.request.GET.get('paginate_by', self.paginate_by)

class OperatingSystemDetails(DetailView):
	template_name = "itam/software/os/os-details.html"
	model = OperatingSystem
	context_object_name = "os_object"

	def get_context_data(self, **kwargs):
		context = super(OperatingSystemDetails, self).get_context_data(**kwargs)
		context['active_page'] = 'os'
		return context

class OperatingSystemCreate(CreateView):
	template_name = "itam/software/os/add-os.html"
	model = OperatingSystem
	form_class = OperatingSystemForm
	success_url = reverse_lazy('itam:os-list')

	def get_context_data(self, **kwargs):
		context = super(OperatingSystemCreate, self).get_context_data(**kwargs)
		context['active_page'] = 'os'
		return context

	def form_valid(self, form):
		self.object = form.save()
		messages.success(self.request, "Asset created successfully.")
		action.send(self.request.user, verb='created', action_object=form.instance)
		return super().form_valid(form)

class OperatingSystemUpdate(UpdateView):
	template_name = "itam/software/os/edit-os.html"
	model = OperatingSystem
	form_class = OperatingSystemForm

	def get_context_data(self, **kwargs):
		context = super(OperatingSystemUpdate, self).get_context_data(**kwargs)
		context['active_page'] = 'os'
		return context

	def form_valid(self, form):
		self.object = form.save()
		messages.success(self.request, "Asset updated successfully.")
		action.send(self.request.user, verb='updated', action_object=form.instance)
		return super().form_valid(form)

class OperatingSystemDelete(DeleteView):
	model = OperatingSystem
	success_url = reverse_lazy('itam:os-list')

	def delete(self, request, *args, **kwargs):
		self.object = self.get_object()
		messages.success(self.request, "Asset deleted successfully.")
		action.send(self.request.user, verb='deleted %s' % self.object)
		self.object.delete()
		return HttpResponseRedirect(self.get_success_url())

class MicrosoftOfficeList(ListView):
	template_name = "itam/software/mo/display-mo.html"
	model = MicrosoftOffice
	context_object_name = "mo_objects"
	paginate_by = 10

	def get_context_data(self, **kwargs):
		context = super(MicrosoftOfficeList, self).get_context_data(**kwargs)
		context['active_page'] = 'mo'
		context['paginate_value'] = self.request.GET.get('paginate_by', self.paginate_by)
		context['filter_value'] = self.request.GET.get("filterInput")
		return context

	def get_queryset(self):
		filter_input = self.request.GET.get("filterInput")
		search_input = self.request.GET.get("searchInput") 
		if self.request.GET:
			if filter_input:
				if filter_input == "All":
					return MicrosoftOffice.objects.filter(is_archived=False)
				else:
					return MicrosoftOffice.objects.filter(Q(is_archived=False, deployment_status__exact=filter_input))
			elif search_input:
				return MicrosoftOffice.objects.filter(Q(is_archived=False, mo_name__icontains=search_input) | 
					Q(is_archived=False, mo_type__icontains=search_input) | 
					Q(is_archived=False, deployment_status__icontains=search_input) |
					Q(is_archived=False, computer__computer_id__icontains=search_input))
		return MicrosoftOffice.objects.filter(is_archived=False)

	def get_paginate_by(self, queryset):
		return self.request.GET.get('paginate_by', self.paginate_by)

class MicrosoftOfficeDetails(DetailView):
	template_name = "itam/software/mo/mo-details.html"
	model = MicrosoftOffice
	context_object_name = "mo_object"

	def get_context_data(self, **kwargs):
		context = super(MicrosoftOfficeDetails, self).get_context_data(**kwargs)
		context['active_page'] = 'mo'
		return context

class MicrosoftOfficeCreate(CreateView):
	template_name = "itam/software/mo/add-mo.html"
	model = MicrosoftOffice
	form_class = MicrosoftOfficeForm
	success_url = reverse_lazy('itam:mo-list')

	def get_context_data(self, **kwargs):
		context = super(MicrosoftOfficeCreate, self).get_context_data(**kwargs)
		context['active_page'] = 'mo'
		return context

	def form_valid(self, form):
		self.object = form.save()
		messages.success(self.request, "Asset created successfully.")
		action.send(self.request.user, verb='created', action_object=form.instance)
		return super().form_valid(form)

class MicrosoftOfficeUpdate(UpdateView):
	template_name = "itam/software/mo/edit-mo.html"
	model = MicrosoftOffice
	form_class = MicrosoftOfficeForm

	def get_context_data(self, **kwargs):
		context = super(MicrosoftOfficeUpdate, self).get_context_data(**kwargs)
		context['active_page'] = 'mo'
		return context

	def form_valid(self, form):
		self.object = form.save()
		messages.success(self.request, "Asset updated successfully.")
		action.send(self.request.user, verb='updated', action_object=form.instance)
		return super().form_valid(form)

class MicrosoftOfficeDelete(DeleteView):
	model = MicrosoftOffice
	success_url = reverse_lazy('itam:mo-list')

	def delete(self, request, *args, **kwargs):
		self.object = self.get_object()
		messages.success(self.request, "Asset deleted successfully.")
		action.send(self.request.user, verb='deleted %s' % self.object)
		self.object.delete()
		return HttpResponseRedirect(self.get_success_url())

class OtherSoftwareList(ListView):
	template_name = "itam/software/others/display-other.html"
	model = OtherSoftware
	context_object_name = "other_objects"
	paginate_by = 10

	def get_context_data(self, **kwargs):
		context = super(OtherSoftwareList, self).get_context_data(**kwargs)
		context['active_page'] = 'others'
		context['paginate_value'] = self.request.GET.get('paginate_by', self.paginate_by)
		context['filter_value'] = self.request.GET.get("filterInput")
		return context

	def get_queryset(self):
		filter_input = self.request.GET.get("filterInput")
		search_input = self.request.GET.get("searchInput") 
		if self.request.GET:
			if filter_input:
				if filter_input == "All":
					return OtherSoftware.objects.filter(is_archived=False)
				else:
					return OtherSoftware.objects.filter(Q(is_archived=False, deployment_status__exact=filter_input))
			elif search_input:
				return OtherSoftware.objects.filter(Q(is_archived=False, software_name__icontains=search_input) | 
					Q(is_archived=False, deployment_status__icontains=search_input) |
					Q(is_archived=False, computer__computer_id__icontains=search_input))
		return OtherSoftware.objects.filter(is_archived=False)

	def get_paginate_by(self, queryset):
		return self.request.GET.get('paginate_by', self.paginate_by)

class OtherSoftwareDetails(DetailView):
	template_name = "itam/software/others/other-details.html"
	model = OtherSoftware
	context_object_name = "other"

	def get_context_data(self, **kwargs):
		context = super(OtherSoftwareDetails, self).get_context_data(**kwargs)
		context['active_page'] = 'others'
		return context

class OtherSoftwareCreate(CreateView):
	template_name = "itam/software/others/add-other.html"
	model = OtherSoftware
	form_class = OtherSoftwareForm
	success_url = reverse_lazy('itam:other-list')

	def get_context_data(self, **kwargs):
		context = super(OtherSoftwareCreate, self).get_context_data(**kwargs)
		context['active_page'] = 'others'
		return context
		
	def form_valid(self, form):
		self.object = form.save()
		messages.success(self.request, "Asset created successfully.")
		action.send(self.request.user, verb='created', action_object=form.instance)
		return super().form_valid(form)

class OtherSoftwareUpdate(UpdateView):
	template_name = "itam/software/others/edit-other.html"
	model = OtherSoftware
	form_class = OtherSoftwareForm

	def get_context_data(self, **kwargs):
		context = super(OtherSoftwareUpdate, self).get_context_data(**kwargs)
		context['active_page'] = 'others'
		return context

	def form_valid(self, form):
		self.object = form.save()
		messages.success(self.request, "Asset updated successfully.")
		action.send(self.request.user, verb='updated', action_object=form.instance)
		return super().form_valid(form)

class OtherSoftwareDelete(DeleteView):
	model = OtherSoftware
	success_url = reverse_lazy('itam:other-list')

	def delete(self, request, *args, **kwargs):
		self.object = self.get_object()
		messages.success(self.request, "Asset deleted successfully.")
		action.send(self.request.user, verb='deleted %s' % self.object)
		self.object.delete()
		return HttpResponseRedirect(self.get_success_url())

class UserList(ListView):
	template_name = "itam/user/display-users.html"
	model = User
	context_object_name = "users"
	paginate_by = 10

	@method_decorator(user_passes_test(lambda user: user.is_admin))
	def dispatch(self, *args, **kwargs):
		return super().dispatch(*args, **kwargs)

	def get_context_data(self, **kwargs):
		context = super(UserList, self).get_context_data(**kwargs)
		context['active_page'] = 'user'
		context['paginate_value'] = self.request.GET.get('paginate_by', self.paginate_by)
		return context

	def get_paginate_by(self, queryset):
		return self.request.GET.get('paginate_by', self.paginate_by)

class UserDetails(DetailView):
	template_name = "itam/user/user-details.html"
	model = User
	context_object_name = "users"

	@method_decorator(user_passes_test(lambda user: user.is_admin))
	def dispatch(self, *args, **kwargs):
		return super().dispatch(*args, **kwargs)

	def get_context_data(self, **kwargs):
		context = super(UserDetails, self).get_context_data(**kwargs)
		context['active_page'] = 'user'
		return context

class UserCreate(CreateView):
	template_name = "itam/user/create-user.html"
	model = User
	form_class = RegisterForm

	@method_decorator(user_passes_test(lambda user: user.is_admin))
	def dispatch(self, *args, **kwargs):
		return super().dispatch(*args, **kwargs)

	def get_context_data(self, **kwargs):
		context = super(UserCreate, self).get_context_data(**kwargs)
		context['active_page'] = 'user'
		return context

	def form_valid(self, form):
		self.object = form.save()
		messages.success(self.request, "User created successfully.")
		return super().form_valid(form)

class UserUpdate(UpdateView):
	template_name = "itam/user/edit-user.html"
	model = User
	form_class = UserProfileChangeForm

	@method_decorator(user_passes_test(lambda user: user.is_admin))
	def dispatch(self, *args, **kwargs):
		return super().dispatch(*args, **kwargs)

	def get_context_data(self, **kwargs):
		context = super(UserUpdate, self).get_context_data(**kwargs)
		context['active_page'] = 'user'
		return context

	def form_valid(self, form):
		self.object = form.save()
		messages.success(self.request, "User details updated successfully.")
		return super().form_valid(form)

class UserDelete(DeleteView):
	model = User
	success_url = reverse_lazy('itam:user-list')

class LocationList(ListView):
	template_name = "itam/location/display-location.html"
	model = Location
	context_object_name = "location_objects"
	paginate_by = 10

	def get_context_data(self, **kwargs):
		context = super(LocationList, self).get_context_data(**kwargs)
		context['active_page'] = 'location'
		context['building'] = Location.objects.values_list('building_name', flat=True).distinct()
		context['paginate_value'] = self.request.GET.get('paginate_by', self.paginate_by)
		return context
		
	def get_paginate_by(self, queryset):
		return self.request.GET.get('paginate_by', self.paginate_by)

class LocationDetails(DetailView):
	template_name = "itam/location/location-details.html"
	model = Location
	#context_object_name = "location_objects"

	def get_context_data(self, **kwargs):
		context = super(LocationDetails, self).get_context_data(**kwargs)
		context['active_page'] = 'location'
		return context

class LocationCreate(CreateView):
	template_name = "itam/location/add-location.html"
	model = Location
	form_class = LocationForm

	def get_context_data(self, **kwargs):
		context = super(LocationCreate, self).get_context_data(**kwargs)
		context['active_page'] = 'location' 
		return context

	def form_valid(self, form):
		self.object = form.save()
		messages.success(self.request, "Location created successfully.")
		action.send(self.request.user, verb='created', action_object=form.instance)
		return super().form_valid(form)

class LocationUpdate(UpdateView):
	template_name = "itam/location/edit-location.html"
	model = Location
	form_class = LocationForm

	def get_context_data(self, **kwargs):
		context = super(LocationUpdate, self).get_context_data(**kwargs)
		context['active_page'] = 'location'
		return context

	def form_valid(self, form):
		self.object = form.save()
		messages.success(self.request, "Location updated successfully.")
		action.send(self.request.user, verb='updated', action_object=form.instance)
		return super().form_valid(form)

class LocationDelete(DeleteView):
	model = Location
	success_url = reverse_lazy('itam:location-list')

	def delete(self, request, *args, **kwargs):
		self.object = self.get_object()
		action.send(self.request.user, verb='deleted %s' % self.object)
		self.object.delete()
		return HttpResponseRedirect(self.get_success_url())

class ComputerTrackDeployed(ListView):
	template_name = "itam/hardware/computer/computer-track-deployed.html"
	model = Computer
	context_object_name = "computer_objects"
	paginate_by = 10

	def get_context_data(self, **kwargs):
		context = super(ComputerTrackDeployed, self).get_context_data(**kwargs)
		context['active_page'] = 'computer_track_d'
		context['deployed_assets'] = Computer.objects.filter(location__isnull=False, is_archived=False)
		context['option'] = self.request.GET.get("option")
		context['buildings'] = Location.objects.values_list('building_name', flat=True).distinct()
		context['paginate_value'] = self.request.GET.get('paginate_by', self.paginate_by)
		return context

	def get_queryset(self):
		option = self.request.GET.get("option")
		qs = Computer.objects.filter(location__building_name=option, is_archived=False).values('location__building_name', 'location__office_name', 'system_specs').annotate(count=Count('system_specs'))
		qs1 = Computer.objects.filter(location__building_name__isnull=False, is_archived=False).values('location__building_name', 'location__office_name', 'system_specs').annotate(count=Count('system_specs'))
		if option == "University":
			return qs1
		elif option == None:
			return ""
		else:
			return qs

	def get_paginate_by(self, queryset):
		return self.request.GET.get('paginate_by', self.paginate_by)

class ComputerTrackExpiring(ListView):
	template_name = "itam/hardware/computer/computer-track-expiring.html"
	model = Computer
	context_object_name = "computer_objects"
	paginate_by = 10

	def get_context_data(self, **kwargs):
		context = super(ComputerTrackExpiring, self).get_context_data(**kwargs)
		context['active_page'] = 'computer_track_e'
		context['option'] = self.request.GET.get("option")
		context['expiring_assets'] = Computer.objects.filter(termination_status=True, is_archived=False, location__isnull=False)
		context['paginate_value'] = self.request.GET.get('paginate_by', self.paginate_by)
		return context

	def get_queryset(self):
		option = self.request.GET.get("option")
		if option == "All":
			return Computer.objects.filter(termination_status=True, is_archived=False, location__isnull=False).order_by('-termination_date')
		elif option == "Desktop":
			return Computer.objects.filter(termination_status=True, is_archived=False, location__isnull=False, hardware_type="Desktop").order_by('-termination_date')
		elif option == "Laptop":
			return Computer.objects.filter(termination_status=True, is_archived=False, location__isnull=False, hardware_type="Laptop").order_by('-termination_date')

		return Computer.objects.filter(termination_status=True, is_archived=False, location__isnull=False).order_by('-termination_date')

	def get_paginate_by(self, queryset):
		return self.request.GET.get('paginate_by', self.paginate_by)

class SfpTrackDeployed(ListView, FormMixin):
	template_name = "itam/hardware/sfp/sfp-track-deployed.html"
	model = Sfp
	context_object_name = "sfp_objects"
	form_class = TrackingFilterForm
	paginate_by = 10

	def get_context_data(self, **kwargs):
		context = super(SfpTrackDeployed, self).get_context_data(**kwargs)
		context['active_page'] = 'sfp_track_d'
		context['option'] = self.request.GET.get("option")
		context['deployed_assets'] = Sfp.objects.filter(network_device__isnull=False, network_device__location__isnull=False, network_device__is_archived=False, is_archived=False)
		context['buildings'] = Location.objects.values_list('building_name', flat=True).distinct()
		context['paginate_value'] = self.request.GET.get('paginate_by', self.paginate_by)
		return context

	def get_queryset(self):
		option = self.request.GET.get("option")
		qs = Sfp.objects.filter(network_device__location__building_name=option, network_device__is_archived=False, 
			is_archived=False).values('network_device__location__building_name', 'network_device__location__office_name', 
			'product_number').annotate(count=Count('product_number'))
		qs1 = Sfp.objects.filter(network_device__location__building_name__isnull=False, network_device__is_archived=False, 
			is_archived=False).values('network_device__location__building_name', 'network_device__location__office_name', 
			'product_number').annotate(count=Count('product_number'))
		if option == "University":
			return qs1
		elif option == None:
			return ""
		else:
			return qs

	def get_paginate_by(self, queryset):
		return self.request.GET.get('paginate_by', self.paginate_by)

class SfpTrackExpiring(ListView):
	template_name = "itam/hardware/sfp/sfp-track-expiring.html"
	model = Sfp
	context_object_name = "sfp_objects"
	paginate_by = 10

	def get_context_data(self, **kwargs):
		context = super(SfpTrackExpiring, self).get_context_data(**kwargs)
		context['active_page'] = 'sfp_track_e'
		context['expiring_assets'] = Sfp.objects.filter(termination_status=True, is_archived=False, network_device__location__isnull=False)
		context['paginate_value'] = self.request.GET.get('paginate_by', self.paginate_by)
		return context

	def get_queryset(self):
		return Sfp.objects.filter(termination_status=True, is_archived=False, network_device__location__isnull=False).order_by('-termination_date')

	def get_paginate_by(self, queryset):
		return self.request.GET.get('paginate_by', self.paginate_by)

class NetworkDeviceTrackDeployed(ListView, FormMixin):
	template_name = "itam/hardware/network_device/network-device-track-deployed.html"
	model = NetworkDevice
	context_object_name = "network_device_objects"
	form_class = TrackingFilterForm
	paginate_by = 10

	def get_context_data(self, **kwargs):
		context = super(NetworkDeviceTrackDeployed, self).get_context_data(**kwargs)
		context['active_page'] = 'network_device_track_d'
		context['deployed_assets'] = NetworkDevice.objects.filter(location__isnull=False, is_archived=False)
		context['paginate_value'] = self.request.GET.get('paginate_by', self.paginate_by)
		context['buildings'] = Location.objects.values_list('building_name', flat=True).distinct()
		context['option'] = self.request.GET.get("option")
		return context

	def get_queryset(self):
		option = self.request.GET.get("option")
		qs = NetworkDevice.objects.filter(location__building_name=option, is_archived=False).values('location__building_name', 'location__office_name', 'category', 'code').annotate(count=Count('code'))
		qs1 = NetworkDevice.objects.filter(location__building_name__isnull=False, is_archived=False).values('location__building_name', 'location__office_name', 'category', 'code').annotate(count=Count('code'))
		if option == "University":
			return qs1
		elif option == None:
			return ""
		else:
			return qs

	def get_paginate_by(self, queryset):
		return self.request.GET.get('paginate_by', self.paginate_by)

class NetworkDeviceTrackExpiring(ListView):
	template_name = "itam/hardware/network_device/network-device-track-expiring.html"
	model = NetworkDevice
	context_object_name = "network_device_objects"
	paginate_by = 10

	def get_context_data(self, **kwargs):
		context = super(NetworkDeviceTrackExpiring, self).get_context_data(**kwargs)
		context['active_page'] = 'network_device_track_e'
		context['expiring_assets'] = NetworkDevice.objects.filter(termination_status=True, is_archived=False, location__isnull=False)
		context['paginate_value'] = self.request.GET.get('paginate_by', self.paginate_by)
		return context

	def get_queryset(self):
		return NetworkDevice.objects.filter(termination_status=True, is_archived=False, location__isnull=False).order_by('-termination_date')

	def get_paginate_by(self, queryset):
		return self.request.GET.get('paginate_by', self.paginate_by)	

class MonitorTrackDeployed(ListView, FormMixin):
	template_name = "itam/hardware/monitor/monitor-track-deployed.html"
	model = Monitor
	context_object_name = "monitor_objects"
	form_class = TrackingFilterForm
	paginate_by = 10

	def get_context_data(self, **kwargs):
		context = super(MonitorTrackDeployed, self).get_context_data(**kwargs)
		context['active_page'] = 'monitor_track_d'
		context['deployed_assets'] = Monitor.objects.filter(computer__isnull=False, computer__location__isnull=False, computer__is_archived=False, is_archived=False)
		context['option'] = self.request.GET.get("option")
		context['paginate_value'] = self.request.GET.get('paginate_by', self.paginate_by)
		context['buildings'] = Location.objects.values_list('building_name', flat=True).distinct()
		return context

	def get_queryset(self):
		option = self.request.GET.get("option")
		qs = Monitor.objects.filter(computer__location__building_name=option, computer__is_archived=False, is_archived=False).values('computer__location__building_name', 'computer__location__office_name', 'model').annotate(count=Count('model'))
		qs1 = Monitor.objects.filter(computer__location__building_name__isnull=False, computer__is_archived=False, is_archived=False).values('computer__location__building_name', 'computer__location__office_name', 'model').annotate(count=Count('model'))
		if option == "University":
			return qs1
		elif option == None:
			return ""
		else:
			return qs

	def get_paginate_by(self, queryset):
		return self.request.GET.get('paginate_by', self.paginate_by)

class MonitorTrackExpiring(ListView):
	template_name = "itam/hardware/monitor/monitor-track-expiring.html"
	model = Monitor
	context_object_name = "monitor_objects"
	paginate_by = 10

	def get_context_data(self, **kwargs):
		context = super(MonitorTrackExpiring, self).get_context_data(**kwargs)
		context['active_page'] = 'monitor_track_e'
		context['option'] = self.request.GET.get("option")
		context['expiring_assets'] = Monitor.objects.filter(termination_status=True, is_archived=False, computer__location__isnull=False)
		context['paginate_value'] = self.request.GET.get('paginate_by', self.paginate_by)
		return context

	def get_queryset(self):
		return Monitor.objects.filter(termination_status=True, is_archived=False, computer__location__isnull=False).order_by('-termination_date')

	def get_paginate_by(self, queryset):
		return self.request.GET.get('paginate_by', self.paginate_by)	

class PrinterTrackDeployed(ListView, FormMixin):
	template_name = "itam/hardware/printer/printer-track-deployed.html"
	model = Printer
	context_object_name = "printer_objects"
	form_class = TrackingFilterForm
	paginate_by = 10

	def get_context_data(self, **kwargs):
		context = super(PrinterTrackDeployed, self).get_context_data(**kwargs)
		context['active_page'] = 'printer_track_d'
		context['deployed_assets'] = Printer.objects.filter(computer__isnull=False, computer__location__isnull=False, computer__is_archived=False, is_archived=False)
		context['option'] = self.request.GET.get("option")
		context['paginate_value'] = self.request.GET.get('paginate_by', self.paginate_by)
		context['buildings'] = Location.objects.values_list('building_name', flat=True).distinct()
		return context

	def get_queryset(self):
		option = self.request.GET.get("option")
		qs = Printer.objects.filter(computer__location__building_name=option, computer__is_archived=False, is_archived=False).values('computer__location__building_name', 'computer__location__office_name', 'model').annotate(count=Count('model'))
		qs1 = Printer.objects.filter(computer__location__building_name__isnull=False, computer__is_archived=False, is_archived=False).values('computer__location__building_name', 'computer__location__office_name', 'model').annotate(count=Count('model'))
		if option == "University":
			return qs1
		elif option == None:
			return ""
		else:
			return qs

	def get_paginate_by(self, queryset):
		return self.request.GET.get('paginate_by', self.paginate_by)

class PrinterTrackExpiring(ListView):
	template_name = "itam/hardware/printer/printer-track-expiring.html"
	model = Printer
	context_object_name = "printer_objects"
	paginate_by = 10

	def get_context_data(self, **kwargs):
		context = super(PrinterTrackExpiring, self).get_context_data(**kwargs)
		context['active_page'] = 'printer_track_e'
		context['option'] = self.request.GET.get("option")
		context['expiring_assets'] = Printer.objects.filter(termination_status=True, is_archived=False, computer__location__isnull=False)
		context['paginate_value'] = self.request.GET.get('paginate_by', self.paginate_by)
		return context

	def get_queryset(self):
		return Printer.objects.filter(termination_status=True, is_archived=False, computer__location__isnull=False).order_by('-termination_date')

	def get_paginate_by(self, queryset):
		return self.request.GET.get('paginate_by', self.paginate_by)

class ScannerTrackDeployed(ListView, FormMixin):
	template_name = "itam/hardware/scanner/scanner-track-deployed.html"
	model = Scanner
	context_object_name = "scanner_objects"
	form_class = TrackingFilterForm
	paginate_by = 10

	def get_context_data(self, **kwargs):
		context = super(ScannerTrackDeployed, self).get_context_data(**kwargs)
		context['active_page'] = 'scanner_track_d'
		context['deployed_assets'] = Scanner.objects.filter(computer__isnull=False, computer__location__isnull=False, computer__is_archived=False, is_archived=False)
		context['option'] = self.request.GET.get("option")
		context['paginate_value'] = self.request.GET.get('paginate_by', self.paginate_by)
		context['buildings'] = Location.objects.values_list('building_name', flat=True).distinct()
		return context

	def get_queryset(self):
		option = self.request.GET.get("option")
		qs = Scanner.objects.filter(computer__location__building_name=option, computer__is_archived=False, is_archived=False).values('computer__location__building_name', 'computer__location__office_name', 'model').annotate(count=Count('model'))
		qs1 = Scanner.objects.filter(computer__location__building_name__isnull=False, computer__is_archived=False, is_archived=False).values('computer__location__building_name', 'computer__location__office_name', 'model').annotate(count=Count('model'))
		if option == "University":
			return qs1
		elif option == None:
			return ""
		else:
			return qs

	def get_paginate_by(self, queryset):
		return self.request.GET.get('paginate_by', self.paginate_by)

class ScannerTrackExpiring(ListView):
	template_name = "itam/hardware/scanner/scanner-track-expiring.html"
	model = Scanner
	context_object_name = "scanner_objects"
	paginate_by = 10

	def get_context_data(self, **kwargs):
		context = super(ScannerTrackExpiring, self).get_context_data(**kwargs)
		context['active_page'] = 'scanner_track_e'
		context['option'] = self.request.GET.get("option")
		context['expiring_assets'] = Scanner.objects.filter(termination_status=True, is_archived=False, computer__location__isnull=False)
		context['paginate_value'] = self.request.GET.get('paginate_by', self.paginate_by)
		return context

	def get_queryset(self):
		return Scanner.objects.filter(termination_status=True, is_archived=False, computer__location__isnull=False).order_by('-termination_date')

	def get_paginate_by(self, queryset):
		return self.request.GET.get('paginate_by', self.paginate_by)

class OperatingSystemTrackDeployed(ListView, FormMixin):
	template_name = "itam/software/os/os-track-deployed.html"
	model = OperatingSystem
	context_object_name = "os_objects"
	form_class = TrackingFilterForm
	paginate_by = 10

	def get_context_data(self, **kwargs):
		context = super(OperatingSystemTrackDeployed, self).get_context_data(**kwargs)
		context['active_page'] = 'os_track_d'
		context['deployed_assets'] = OperatingSystem.objects.filter(computer__isnull=False, computer__location__isnull=False, computer__is_archived=False, is_archived=False)
		context['option'] = self.request.GET.get("option")
		context['buildings'] = Location.objects.values_list('building_name', flat=True).distinct()
		context['paginate_value'] = self.request.GET.get('paginate_by', self.paginate_by)
		return context

	def get_queryset(self):
		option = self.request.GET.get("option")
		qs = OperatingSystem.objects.filter(computer__location__building_name=option, computer__is_archived=False, is_archived=False).values('computer__location__building_name', 'computer__location__office_name', 'os_name').annotate(count=Count('os_name'))
		qs1 = OperatingSystem.objects.filter(computer__location__building_name__isnull=False, computer__is_archived=False, is_archived=False).values('computer__location__building_name', 'computer__location__office_name', 'os_name').annotate(count=Count('os_name'))
		if option == "University":
			return qs1
		elif option == None:
			return ""
		else:
			return qs

	def get_paginate_by(self, queryset):
		return self.request.GET.get('paginate_by', self.paginate_by)

class OtherSoftwareTrackDeployed(ListView, FormMixin):
	template_name = "itam/software/others/others-track-deployed.html"
	model = OtherSoftware
	context_object_name = "other_objects"
	form_class = TrackingFilterForm
	paginate_by = 10

	def get_context_data(self, **kwargs):
		context = super(OtherSoftwareTrackDeployed, self).get_context_data(**kwargs)
		context['active_page'] = 'others_track_d'
		context['deployed_assets'] = OtherSoftware.objects.filter(computer__isnull=False, computer__location__isnull=False, computer__is_archived=False, is_archived=False)
		context['option'] = self.request.GET.get("option")
		context['paginate_value'] = self.request.GET.get('paginate_by', self.paginate_by)
		context['buildings'] = Location.objects.values_list('building_name', flat=True).distinct()

		return context

	def get_queryset(self):
		option = self.request.GET.get("option")
		qs = OtherSoftware.objects.filter(computer__location__building_name=option, computer__is_archived=False, is_archived=False).values('computer__location__building_name', 'computer__location__office_name', 'software_name').annotate(count=Count('software_name'))
		qs1 = OtherSoftware.objects.filter(computer__location__building_name__isnull=False, computer__is_archived=False, is_archived=False).values('computer__location__building_name', 'computer__location__office_name', 'software_name').annotate(count=Count('software_name'))
		if option == "University":
			return qs1
		elif option == None:
			return ""
		else:
			return qs

	def get_paginate_by(self, queryset):
		return self.request.GET.get('paginate_by', self.paginate_by)

class OtherSoftwareTrackExpiring(ListView):
	template_name = "itam/software/others/others-track-expiring.html"
	model = OtherSoftware
	context_object_name = "other_objects"
	paginate_by = 10

	def get_context_data(self, **kwargs):
		context = super(OtherSoftwareTrackExpiring, self).get_context_data(**kwargs)
		context['active_page'] = 'others_track_e'
		context['option'] = self.request.GET.get("option")
		context['expiring_assets'] = OtherSoftware.objects.filter(expiration_status=True, is_archived=False, computer__location__isnull=False)
		context['paginate_value'] = self.request.GET.get('paginate_by', self.paginate_by)
		return context

	def get_queryset(self):
		return OtherSoftware.objects.filter(expiration_status=True, is_archived=False, computer__location__isnull=False).order_by('-expiration_date')

	def get_paginate_by(self, queryset):
		return self.request.GET.get('paginate_by', self.paginate_by)

class MicrosoftOfficeTrackDeployed(ListView, FormMixin):
	template_name = "itam/software/mo/mo-track-deployed.html"
	model = MicrosoftOffice
	context_object_name = "mo_objects"
	form_class = TrackingFilterForm
	paginate_by = 10

	def get_context_data(self, **kwargs):
		context = super(MicrosoftOfficeTrackDeployed, self).get_context_data(**kwargs)
		context['active_page'] = 'mo_track_d'
		context['deployed_assets'] = MicrosoftOffice.objects.filter(computer__isnull=False, computer__location__isnull=False, computer__is_archived=False, is_archived=False)
		context['option'] = self.request.GET.get("option")
		context['paginate_value'] = self.request.GET.get('paginate_by', self.paginate_by)
		context['buildings'] = Location.objects.values_list('building_name', flat=True).distinct()
		return context

	def get_queryset(self):
		option = self.request.GET.get("option")
		qs = MicrosoftOffice.objects.filter(computer__location__building_name=option, computer__is_archived=False, is_archived=False).values('computer__location__building_name', 'computer__location__office_name', 'mo_name').annotate(count=Count('mo_name'))
		qs1 = MicrosoftOffice.objects.filter(computer__location__building_name__isnull=False, computer__is_archived=False, is_archived=False).values('computer__location__building_name', 'computer__location__office_name', 'mo_name').annotate(count=Count('mo_name'))
		if option == "University":
			return qs1
		elif option == None:
			return ""
		else:
			return qs

	def get_paginate_by(self, queryset):
		return self.request.GET.get('paginate_by', self.paginate_by)

#Displays Computer Archive
class ComputerArchiveList(ListView):
	template_name = "itam/hardware/computer/computer-archive.html"
	model = Computer
	context_object_name = "computers"
	paginate_by = 10

	def get_context_data(self, **kwargs):
		context = super(ComputerArchiveList, self).get_context_data(**kwargs)
		context['active_page'] = 'computer'
		context['filter_value'] = self.request.GET.get("filterInput")
		context['paginate_value'] = self.request.GET.get('paginate_by', self.paginate_by)
		return context

	def get_queryset(self):
		filter_input = self.request.GET.get("filterInput")
		search_input = self.request.GET.get("searchInput") 
		if self.request.GET:
			if filter_input:
				if filter_input == "All":
					return Computer.objects.filter(is_archived=True)
				else:	
					return Computer.objects.filter(Q(is_archived=True, deployment_status__search=filter_input))
			elif search_input:
				return Computer.objects.filter(Q(is_archived=True, computer_id__icontains=search_input) | 
					Q(is_archived=True, hardware_type__icontains=search_input) | 
					Q(is_archived=True, deployment_status__icontains=search_input) |
					Q(is_archived=True, termination_date__icontains=search_input))
		return Computer.objects.filter(is_archived=True)

	def get_paginate_by(self, queryset):
		return self.request.GET.get('paginate_by', self.paginate_by)

def archive_computer(request, pk):
	"""
	Sets a Computer instance's 'is_archived' field to TRUE
	"""
	selected_computer = Computer.objects.get(pk=request.POST['computer.id'])
	action.send(request.user, verb='archived', action_object=selected_computer)
	messages.success(request, "Asset archived successfully.")
	selected_computer.is_archived = True 
	
	if selected_computer.operating_system:
		selected_computer.operating_system.is_archived = True
		selected_computer.operating_system.save() 

	if selected_computer.microsoft_office:
		selected_computer.microsoft_office.is_archived = True
		selected_computer.microsoft_office.save()

	if selected_computer.other_software:
		selected_computer.other_software.is_archived = True 
		selected_computer.other_software.save()

	if selected_computer.monitor:
		selected_computer.monitor.is_archived = True 
		selected_computer.monitor.save()

	if selected_computer.printer:
		selected_computer.printer.is_archived = True 
		selected_computer.printer.save()

	if selected_computer.scanner:
		selected_computer.scanner.is_archived = True
		selected_computer.scanner.save()

	selected_computer.save()
	return redirect('itam:computer-list')

def unarchive_computer(request, pk):
	"""
	Sets a Computer instance's 'is_archived' field to TRUE
	"""
	selected_computer = Computer.objects.get(pk=request.POST['computer.id'])
	action.send(request.user, verb='unarchived', action_object=selected_computer)
	messages.success(request, '%s and its related objects were restored successfully.' % selected_computer)
	selected_computer.is_archived = False 
	
	if selected_computer.operating_system:
		selected_computer.operating_system.is_archived = False
		selected_computer.operating_system.save() 

	if selected_computer.microsoft_office:
		selected_computer.microsoft_office.is_archived = False
		selected_computer.microsoft_office.save()

	if selected_computer.other_software:
		selected_computer.other_software.is_archived = False 
		selected_computer.other_software.save()

	if selected_computer.monitor:
		selected_computer.monitor.is_archived = False 
		selected_computer.monitor.save()

	if selected_computer.printer:
		selected_computer.printer.is_archived = False 
		selected_computer.printer.save()

	if selected_computer.scanner:
		selected_computer.scanner.is_archived = False
		selected_computer.scanner.save()

	selected_computer.save()
	return redirect('itam:computer-archive')

class MonitorArchiveList(ListView):
	"""
	Displays Monitor Archive List
	"""
	template_name = "itam/hardware/monitor/monitor-archive.html"
	model = Monitor
	context_object_name = "monitors"
	paginate_by = 10

	def get_context_data(self, **kwargs):
		context = super(MonitorArchiveList, self).get_context_data(**kwargs)
		context['active_page'] = 'monitor'
		context['paginate_value'] = self.request.GET.get('paginate_by', self.paginate_by)
		context['filter_value'] = self.request.GET.get("filterInput")
		return context

	def get_queryset(self):
		filter_input = self.request.GET.get("filterInput")
		search_input = self.request.GET.get("searchInput") 
		if self.request.GET:
			if filter_input:
				if filter_input == "All":
					return Monitor.objects.filter(is_archived=True)
				else:
					return Monitor.objects.filter(Q(is_archived=True, deployment_status__search=filter_input))
			elif search_input:
				return Monitor.objects.filter(Q(is_archived=True, model__icontains=search_input) | 
					Q(is_archived=True, brand__icontains=search_input) | 
					Q(is_archived=True, deployment_status__icontains=search_input) |
					Q(is_archived=True, termination_date__icontains=search_input) |
					Q(is_archived=True, computer__computer_id__icontains=search_input))
		return Monitor.objects.filter(is_archived=True)

	def get_paginate_by(self, queryset):
		return self.request.GET.get('paginate_by', self.paginate_by)

#Archives a monitor
def archive_monitor(request, pk):
	selected_monitor = Monitor.objects.get(pk=request.POST['monitor.id'])
	action.send(request.user, verb='archived', action_object=selected_monitor)
	messages.success(request, "Asset restored successfully.")
	selected_monitor.is_archived = True
	selected_monitor.save()
	for computer in Computer.objects.filter(monitor__isnull=False, monitor__is_archived=True):
		computer.monitor.deployment_status = "Undeployed"
		computer.monitor = None
		computer.save()
	return redirect('itam:monitor-list')

#Restores archived monitor
def unarchive_monitor(request, pk):
	selected_monitor = Monitor.objects.get(pk=request.POST['monitor.id'])
	action.send(request.user, verb='unarchived', action_object=selected_monitor)
	messages.success(request, "Asset restored successfully.")
	selected_monitor.is_archived = False
	selected_monitor.save()
	return redirect('itam:monitor-archive')

class PrinterArchiveList(ListView):
	"""
	Displays all archived printers
	"""
	template_name = "itam/hardware/printer/printer-archive.html"
	model = Printer
	context_object_name = "printers"
	paginate_by = 10

	def get_context_data(self, **kwargs):
		context = super(PrinterArchiveList, self).get_context_data(**kwargs)
		context['active_page'] = 'printer'
		context['paginate_value'] = self.request.GET.get('paginate_by', self.paginate_by)
		context['filter_value'] = self.request.GET.get("filterInput")
		return context

	def get_queryset(self):
		filter_input = self.request.GET.get("filterInput")
		search_input = self.request.GET.get("searchInput") 
		if self.request.GET:
			if filter_input:
				if filter_input == "All":
					return Printer.objects.filter(is_archived=True)
				else:
					return Printer.objects.filter(Q(is_archived=True, deployment_status__search=filter_input))
			elif search_input:
				return Printer.objects.filter(Q(is_archived=True, model__icontains=search_input) | 
					Q(is_archived=True, brand__icontains=search_input) | 
					Q(is_archived=True, deployment_status__icontains=search_input) |
					Q(is_archived=True, termination_date__icontains=search_input) |
					Q(is_archived=True, computer__computer_id__icontains=search_input))
		return Printer.objects.filter(is_archived=True)

	def get_paginate_by(self, queryset):
		return self.request.GET.get('paginate_by', self.paginate_by)

#Archives a printer
def archive_printer(request, pk):
	selected_printer = Printer.objects.get(pk=request.POST['printer.id'])
	action.send(request.user, verb='archived', action_object=selected_printer)
	messages.success(request, "Asset archived successfully.")
	selected_printer.is_archived = True
	selected_printer.save()
	for computer in Computer.objects.filter(printer__isnull=False, printer__is_archived=True):
		computer.printer.deployment_status = "Undeployed"
		computer.printer = None
		computer.save()
	return redirect('itam:printer-list')

#Restores archived printer
def unarchive_printer(request, pk):
	selected_printer = Printer.objects.get(pk=request.POST['printer.id'])
	action.send(request.user, verb='unarchived', action_object=selected_printer)
	messages.success(request, "Asset restored successfully.")
	selected_printer.is_archived = False
	selected_printer.save()
	return redirect('itam:printer-archive')


class ScannerArchiveList(ListView):
	"""
	Displays all archived scanners
	"""
	template_name = "itam/hardware/scanner/scanner-archive.html"
	model = Scanner
	context_object_name = "scanners"
	paginate_by = 10

	def get_context_data(self, **kwargs):
		context = super(ScannerArchiveList, self).get_context_data(**kwargs)
		context['active_page'] = 'scanner'
		context['paginate_value'] = self.request.GET.get('paginate_by', self.paginate_by)
		context['filter_value'] = self.request.GET.get("filterInput")
		return context

	def get_queryset(self):
		filter_input = self.request.GET.get("filterInput")
		search_input = self.request.GET.get("searchInput") 
		if self.request.GET:
			if filter_input:
				if filter_input == "All":
					return Scanner.objects.filter(is_archived=True)
				else:
					return Scanner.objects.filter(Q(is_archived=True, deployment_status__search=filter_input))
			elif search_input:
				return Scanner.objects.filter(Q(is_archived=True, model__icontains=search_input) | 
					Q(is_archived=True, brand__icontains=search_input) | 
					Q(is_archived=True, deployment_status__icontains=search_input) |
					Q(is_archived=True, termination_date__icontains=search_input) |
					Q(is_archived=True, computer__computer_id__icontains=search_input))
		return Scanner.objects.filter(is_archived=True)

	def get_paginate_by(self, queryset):
		return self.request.GET.get('paginate_by', self.paginate_by)

def archive_scanner(request, pk):
	selected_scanner = Scanner.objects.get(pk=request.POST['scanner.id'])
	action.send(request.user, verb='archived', action_object=selected_scanner)
	messages.success(request, "Asset archived successfully.")
	selected_scanner.is_archived = True
	selected_scanner.save()
	for computer in Computer.objects.filter(scanner__isnull=False, scanner__is_archived=True):
		computer.scanner.deployment_status = "Undeployed"
		computer.scanner = None
		computer.save()
	return redirect('itam:scanner-list')

def unarchive_scanner(request, pk):
	selected_scanner = Scanner.objects.get(pk=request.POST['scanner.id'])
	action.send(request.user, verb='unarchived', action_object=selected_scanner)
	messages.success(request, "Asset restored successfully.")
	selected_scanner.is_archived = False
	selected_scanner.save()
	return redirect('itam:scanner-archive')

class SfpArchiveList(ListView):
	template_name = "itam/hardware/sfp/sfp-archive.html"
	model = Sfp
	context_object_name = "sfps"
	paginate_by = 10

	def get_context_data(self, **kwargs):
		context = super(SfpArchiveList, self).get_context_data(**kwargs)
		context['active_page'] = 'sfp'
		context['paginate_value'] = self.request.GET.get('paginate_by', self.paginate_by)
		context['filter_value'] = self.request.GET.get("filterInput")
		return context

	def get_queryset(self):
		filter_input = self.request.GET.get("filterInput")
		search_input = self.request.GET.get("searchInput") 
		if self.request.GET:
			if filter_input:
				if filter_input == "All":
					return Sfp.objects.filter(is_archived=True)
				else:
					return Sfp.objects.filter(Q(is_archived=True, deployment_status__exact=filter_input))
			elif search_input:
				return Sfp.objects.filter(Q(is_archived=True, product_number__icontains=search_input) | 
					Q(is_archived=True, manufacturer__icontains=search_input) | 
					Q(is_archived=True, deployment_status__icontains=search_input) |
					Q(is_archived=True, termination_date__icontains=search_input) |
					Q(is_archived=True, network_device__code__icontains=search_input))
		return Sfp.objects.filter(is_archived=True)

	def get_paginate_by(self, queryset):
		return self.request.GET.get('paginate_by', self.paginate_by)

def archive_sfp(request, pk):
	selected_sfp = Sfp.objects.get(pk=request.POST['sfp.id'])
	action.send(request.user, verb='archived', action_object=selected_sfp)
	messages.success(request, "Asset archived successfully.")
	selected_sfp.is_archived = True
	selected_sfp.save()
	for nd in NetworkDevice.objects.filter(sfp__isnull=False, sfp__is_archived=True):
		nd.sfp_set.filter(is_archived=True).update(deployment_status = "Not Installed", network_device=None)
	return redirect('itam:sfp-list')

def unarchive_sfp(request, pk):
	selected_sfp = Sfp.objects.get(pk=request.POST['sfp.id'])
	action.send(request.user, verb='unarchived', action_object=selected_sfp)
	messages.success(request, "Asset restored successfully.")
	selected_sfp.is_archived = False
	selected_sfp.save()
	return redirect('itam:sfp-archive')

class NetworkDeviceArchiveList(ListView):
	template_name = "itam/hardware/network_device/network-device-archive.html"
	model = NetworkDevice
	context_object_name = "network_devices"
	paginate_by = 10

	def get_context_data(self, **kwargs):
		context = super(NetworkDeviceArchiveList, self).get_context_data(**kwargs)
		context['active_page'] = 'network_device'
		context['filter_value'] = self.request.GET.get("filterInput")
		context['paginate_value'] = self.request.GET.get('paginate_by', self.paginate_by)
		return context

	def get_queryset(self):
		filter_input = self.request.GET.get("filterInput")
		search_input = self.request.GET.get("searchInput") 
		if self.request.GET:
			if filter_input:
				if filter_input == "All":
					return NetworkDevice.objects.filter(is_archived=True)
				else:	
					return NetworkDevice.objects.filter(Q(is_archived=True, deployment_status__search=filter_input))
			elif search_input:
				return NetworkDevice.objects.filter(Q(is_archived=True, code__icontains=search_input) | 
					Q(is_archived=True, category__icontains=search_input) | 
					Q(is_archived=True, deployment_status__icontains=search_input) |
					Q(is_archived=True, termination_date__icontains=search_input) |
					Q(is_archived=True, location__building_name__icontains=search_input)|
					Q(is_archived=True, location__office_name__icontains=search_input))
		return NetworkDevice.objects.filter(is_archived=True)

	def get_paginate_by(self, queryset):
		return self.request.GET.get('paginate_by', self.paginate_by)

def archive_network_device(request, pk):
	selected_nd = NetworkDevice.objects.get(pk=request.POST['nd.id'])
	action.send(request.user, verb='archived', action_object=selected_nd)
	messages.success(request, "Asset archived successfully.")
	selected_nd.is_archived = True
	selected_nd.save()
	selected_nd.sfp_set.all().update(is_archived=True)
	return redirect('itam:network-device-list')

def unarchive_network_device(request, pk):
	selected_nd = NetworkDevice.objects.get(pk=request.POST['nd.id'])
	action.send(request.user, verb='unarchived', action_object=selected_nd)
	messages.success(request, "Asset restored successfully.")
	selected_nd.is_archived = False
	selected_nd.save()
	selected_nd.sfp_set.all().update(is_archived=False)
	return redirect('itam:network-device-archive')

class OperatingSystemArchiveList(ListView):
	template_name = "itam/software/os/os-archive.html"
	model = OperatingSystem
	context_object_name = "os_objects"
	paginate_by = 10

	def get_context_data(self, **kwargs):
		context = super(OperatingSystemArchiveList, self).get_context_data(**kwargs)
		context['active_page'] = 'os'
		context['paginate_value'] = self.request.GET.get('paginate_by', self.paginate_by)
		context['filter_value'] = self.request.GET.get("filterInput")
		return context

	def get_queryset(self):
		filter_input = self.request.GET.get("filterInput")
		search_input = self.request.GET.get("searchInput") 
		if self.request.GET:
			if filter_input:
				if filter_input == "All":
					return OperatingSystem.objects.filter(is_archived=True)
				else:
					return OperatingSystem.objects.filter(Q(is_archived=True, deployment_status__exact=filter_input))
			elif search_input:
				return OperatingSystem.objects.filter(Q(is_archived=True, os_name__icontains=search_input) | 
					Q(is_archived=True, os_type__icontains=search_input) | 
					Q(is_archived=True, deployment_status__icontains=search_input) |
					Q(is_archived=True, computer__computer_id__icontains=search_input))
		return OperatingSystem.objects.filter(is_archived=True)

	def get_paginate_by(self, queryset):
		return self.request.GET.get('paginate_by', self.paginate_by)

def archive_os(request, pk):
	selected_os = OperatingSystem.objects.get(pk=request.POST['os.id'])
	action.send(request.user, verb='archived', action_object=selected_os)
	messages.success(request, "Asset archived successfully.")
	selected_os.is_archived = True
	selected_os.save()
	for computer in Computer.objects.filter(operating_system__isnull=False, operating_system__is_archived=True):
		computer.operating_system.deployment_status = "Not Installed"
		computer.operating_system = None
		computer.save()
	return redirect('itam:os-list')

def unarchive_os(request, pk):
	selected_os = OperatingSystem.objects.get(pk=request.POST['os.id'])
	action.send(request.user, verb='unarchived', action_object=selected_os)
	messages.success(request, "Asset restored successfully.")
	selected_os.is_archived = False
	selected_os.save()
	return redirect('itam:os-archive')

class MicrosoftOfficeArchiveList(ListView):
	template_name = "itam/software/mo/mo-archive.html"
	model = MicrosoftOffice
	context_object_name = "mo_objects"
	paginate_by = 10

	def get_context_data(self, **kwargs):
		context = super(MicrosoftOfficeArchiveList, self).get_context_data(**kwargs)
		context['active_page'] = 'mo'
		context['paginate_value'] = self.request.GET.get('paginate_by', self.paginate_by)
		context['filter_value'] = self.request.GET.get("filterInput")
		return context

	def get_queryset(self):
		filter_input = self.request.GET.get("filterInput")
		search_input = self.request.GET.get("searchInput") 
		if self.request.GET:
			if filter_input:
				if filter_input == "All":
					return MicrosoftOffice.objects.filter(is_archived=True)
				else:
					return MicrosoftOffice.objects.filter(Q(is_archived=True, deployment_status__exact=filter_input))
			elif search_input:
				return MicrosoftOffice.objects.filter(Q(is_archived=True, mo_name__icontains=search_input) | 
					Q(is_archived=True, mo_type__icontains=search_input) | 
					Q(is_archived=True, deployment_status__icontains=search_input) |
					Q(is_archived=True, computer__computer_id__icontains=search_input))
		return MicrosoftOffice.objects.filter(is_archived=True)

	def get_paginate_by(self, queryset):
		return self.request.GET.get('paginate_by', self.paginate_by)

def archive_mo(request, pk):
	selected_mo = MicrosoftOffice.objects.get(pk=request.POST['mo.id'])
	action.send(request.user, verb='archived', action_object=selected_mo)
	messages.success(request, "Asset archived successfully.")
	selected_mo.is_archived = True
	selected_mo.save()
	#Sets 
	for computer in Computer.objects.filter(microsoft_office__isnull=False, microsoft_office__is_archived=True):
		computer.microsoft_office.deployment_status = "Not Installed"
		computer.microsoft_office = None
		computer.save()
	return redirect('itam:mo-list')

def unarchive_mo(request, pk):
	selected_mo = MicrosoftOffice.objects.get(pk=request.POST['mo.id'])
	action.send(request.user, verb='unarchived', action_object=selected_mo)
	messages.success(request, "Asset restored successfully.")
	selected_mo.is_archived = False
	selected_mo.save()
	return redirect('itam:mo-archive')

class OtherSoftwareArchiveList(ListView):
	template_name = "itam/software/others/other-archive.html"
	model = OtherSoftware
	context_object_name = "other_objects"
	paginate_by = 10

	def get_context_data(self, **kwargs):
		context = super(OtherSoftwareArchiveList, self).get_context_data(**kwargs)
		context['active_page'] = 'others'
		context['paginate_value'] = self.request.GET.get('paginate_by', self.paginate_by)
		context['filter_value'] = self.request.GET.get("filterInput")
		return context

	def get_queryset(self):
		filter_input = self.request.GET.get("filterInput")
		search_input = self.request.GET.get("searchInput") 
		if self.request.GET:
			if filter_input:
				if filter_input == "All":
					return OtherSoftware.objects.filter(is_archived=True)
				else:
					return OtherSoftware.objects.filter(Q(is_archived=True, deployment_status__exact=filter_input))
			elif search_input:
				return OtherSoftware.objects.filter(Q(is_archived=True, software_name__icontains=search_input) | 
					Q(is_archived=True, deployment_status__icontains=search_input) |
					Q(is_archived=True, computer__computer_id__icontains=search_input))
		return OtherSoftware.objects.filter(is_archived=True)

	def get_paginate_by(self, queryset):
		return self.request.GET.get('paginate_by', self.paginate_by)

def archive_other(request, pk):
	selected_other = OtherSoftware.objects.get(pk=request.POST['other.id'])
	action.send(request.user, verb='archived', action_object=selected_other)
	messages.success(request, "Asset archived successfully.")
	selected_other.is_archived = True
	selected_other.save()
	selected_other.computer.delete()
	return redirect('itam:other-list')

def unarchive_other(request, pk):
	selected_other = MicrosoftOffice.objects.get(pk=request.POST['other.id'])
	action.send(request.user, verb='unarchived', action_object=selected_other)
	messages.success(request, "Asset restored successfully.")
	selected_other.is_archived = False
	selected_other.save()
	return redirect('itam:other-archive')

def generate_computer_qr(request, pk):
	data = serializers.serialize('json', Computer.objects.filter(pk=request.POST['computer.id']),  
	fields=('computer_id', 'hardware_type', 'system_specs', 'base_license_os',
		'operating_system', 'microsoft_office', 'deployment_date', 'location',
		'other_software'),
		use_natural_foreign_keys=True, indent=4)
	
	#Decodes serialized data json obj to a python dict
	raw_data = json.loads(data)
	
	#Deletes PK and Model Keys 
	req_data = []
	for d in raw_data:
		del d['pk']
		del d['model']
		req_data.append(d)
	
	#Get and store keys and values to a string buffer
	cleaned_data = StringIO()
	for fields_key in req_data:
		for key, values in fields_key['fields'].items():
			cleaned_data.write("%s: %s" % (key, values))
			cleaned_data.write("\n")
		
	a = json.loads(json.dumps(cleaned_data.getvalue()))
	qr = pyqrcode.create(a)
	buf = BytesIO()
	qr.png(buf, scale=5)
	contents = base64.b64encode(buf.getvalue()).decode()
	mime = "image/png"
	uri = "data:%s;base64,%s" % (mime, contents)
	return HttpResponse('<img src="' + uri + '"/>')

def generate_monitor_qr(request, pk):
	data = serializers.serialize('json', Monitor.objects.filter(pk=request.POST['m.id']),  
		fields=('model', 'brand', 'serial_number', 'deployment_date'),
		use_natural_foreign_keys=True, indent=4)
	
	#Decodes serialized data json obj to a python dict
	raw_data = json.loads(data)
	
	#Deletes PK and Model Keys 
	req_data = []
	for d in raw_data:
		del d['pk']
		del d['model']
		req_data.append(d)
	
	#Get and store keys and values to a string buffer
	cleaned_data = StringIO()
	for fields_key in req_data:
		for key, values in fields_key['fields'].items():
			cleaned_data.write("%s: %s" % (key, values))
			cleaned_data.write("\n")
		
	a = json.loads(json.dumps(cleaned_data.getvalue()))
	qr = pyqrcode.create(a)
	buf = BytesIO()
	qr.png(buf, scale=5)
	contents = base64.b64encode(buf.getvalue()).decode()
	mime = "image/png"
	uri = "data:%s;base64,%s" % (mime, contents)
	return HttpResponse('<img src="' + uri + '"/>')

def generate_printer_qr(request, pk):
	data = serializers.serialize('json', Printer.objects.filter(pk=request.POST['m.id']),  
		fields=('model', 'brand', 'serial_number', 'deployment_date'),
		use_natural_foreign_keys=True, indent=4)
	
	#Decodes serialized data json obj to a python dict
	raw_data = json.loads(data)
	
	#Deletes PK and Model Keys 
	req_data = []
	for d in raw_data:
		del d['pk']
		del d['model']
		req_data.append(d)
	
	#Get and store keys and values to a string buffer
	cleaned_data = StringIO()
	for fields_key in req_data:
		for key, values in fields_key['fields'].items():
			cleaned_data.write("%s: %s" % (key, values))
			cleaned_data.write("\n")
		
	a = json.loads(json.dumps(cleaned_data.getvalue()))
	qr = pyqrcode.create(a)
	buf = BytesIO()
	qr.png(buf, scale=5)
	contents = base64.b64encode(buf.getvalue()).decode()
	mime = "image/png"
	uri = "data:%s;base64,%s" % (mime, contents)
	return HttpResponse('<img src="' + uri + '"/>')

def generate_scanner_qr(request, pk):
	data = serializers.serialize('json', Scanner.objects.filter(pk=request.POST['m.id']),  
		fields=('model', 'brand', 'serial_number', 'deployment_date'),
		use_natural_foreign_keys=True, indent=4)
	
	#Decodes serialized data json obj to a python dict
	raw_data = json.loads(data)
	
	#Deletes PK and Model Keys 
	req_data = []
	for d in raw_data:
		del d['pk']
		del d['model']
		req_data.append(d)
	
	#Get and store keys and values to a string buffer
	cleaned_data = StringIO()
	for fields_key in req_data:
		for key, values in fields_key['fields'].items():
			cleaned_data.write("%s: %s" % (key, values))
			cleaned_data.write("\n")
		
	a = json.loads(json.dumps(cleaned_data.getvalue()))
	qr = pyqrcode.create(a)
	buf = BytesIO()
	qr.png(buf, scale=5)
	contents = base64.b64encode(buf.getvalue()).decode()
	mime = "image/png"
	uri = "data:%s;base64,%s" % (mime, contents)
	return HttpResponse('<img src="' + uri + '"/>')

def generate_network_device_qr(request, pk):
	data = serializers.serialize('json', NetworkDevice.objects.filter(pk=request.POST['nd.id']),  
		fields=('category', 'code', 'management_ip', 'warranty_date', 'location'),
		use_natural_foreign_keys=True, indent=4)
		
	#Decodes serialized data json obj to a python dict
	raw_data = json.loads(data)
	
	#Deletes PK and Model Keys 
	req_data = []
	for d in raw_data:
		del d['pk']
		del d['model']
		req_data.append(d)
	
	#Get and store keys and values to a string buffer
	cleaned_data = StringIO()
	for fields_key in req_data:
		for key, values in fields_key['fields'].items():
			cleaned_data.write("%s: %s" % (key, values))
			cleaned_data.write("\n")
		
	a = json.loads(json.dumps(cleaned_data.getvalue()))
	qr = pyqrcode.create(a)
	buf = BytesIO()
	qr.png(buf, scale=5)
	contents = base64.b64encode(buf.getvalue()).decode()
	mime = "image/png"
	uri = "data:%s;base64,%s" % (mime, contents)
	return HttpResponse('<img src="' + uri + '"/>')

class ComputerGenerateReport(View):
	def get(self, request):

		option = self.request.GET.get("option")
		if option == "University":
			query = Computer.objects.filter(location__isnull=False, is_archived=False).values(
				'system_specs').annotate(count=Count('system_specs'))
		elif option == None:
			query = None
		else:
			query = Computer.objects.filter(location__building_name=option, location__isnull=False, is_archived=False).values(
				'location__building_name' ,'location__office_name','system_specs').annotate(count=Count('system_specs'))

		today = timezone.now()
		params = {
			'today': today,
			'option': option,
			'query': query,
			'request': request
		}
		return Render.render('itam/hardware/computer/computer-pdf-report.html', params)

class ComputerGenerateReportExpired(View):
	def get(self, request):

		query = Computer.objects.filter(termination_status=True, is_archived=False, location__isnull=False).order_by('-termination_date')

		today = timezone.now()
		params = {
			'today': today,
			'query': query,
			'request': request
		}
		return Render.render('itam/hardware/computer/e-computer-pdf-report.html', params)

class MonitorGenerateReport(View):
	def get(self, request):

		option = self.request.GET.get("option")
		if option == "University":
			query = Monitor.objects.filter(computer__location__isnull=False, computer__is_archived=False, 
				is_archived=False).values('model').annotate(count=Count('model'))
		elif option == None:
			query = None
		else:
			query = Monitor.objects.filter(computer__location__building_name=option,computer__location__isnull=False, 
				computer__is_archived=False, is_archived=False).values('computer__location__building_name', 
				'computer__location__office_name' ,'model').annotate(count=Count('model'))

		today = timezone.now()
		params = {
			'today': today,
			'option': option,
			'query': query,
			'request': request
		}
		return Render.render('itam/hardware/monitor/monitor-pdf-report.html', params)

class MonitorGenerateReportExpired(View):
	def get(self, request):

		query = Monitor.objects.filter(termination_status=True, is_archived=False, computer__location__isnull=False).order_by('-termination_date')

		today = timezone.now()
		params = {
			'today': today,
			'query': query,
			'request': request
		}
		return Render.render('itam/hardware/monitor/e-monitor-pdf-report.html', params)

class PrinterGenerateReport(View):
	def get(self, request):

		option = self.request.GET.get("option")
		if option == "University":
			query = Printer.objects.filter(computer__location__isnull=False, computer__is_archived=False, 
				is_archived=False).values('model').annotate(count=Count('model'))
		elif option == None:
			query = None
		else:
			query = Printer.objects.filter(computer__location__building_name=option, computer__location__isnull=False, 
				computer__is_archived=False, is_archived=False).values('computer__location__building_name', 
				'computer__location__office_name' ,'model').annotate(count=Count('model'))

		today = timezone.now()
		params = {
			'today': today,
			'option': option,
			'query': query,
			'request': request
		}
		return Render.render('itam/hardware/printer/printer-pdf-report.html', params)

class PrinterGenerateReportExpired(View):
	def get(self, request):

		query = Printer.objects.filter(termination_status=True, is_archived=False, computer__location__isnull=False).order_by('-termination_date')

		today = timezone.now()
		params = {
			'today': today,
			'query': query,
			'request': request
		}
		return Render.render('itam/hardware/printer/e-printer-pdf-report.html', params)

class ScannerGenerateReport(View):
	def get(self, request):

		option = self.request.GET.get("option")
		if option == "University":
			query = Scanner.objects.filter(computer__location__isnull=False, computer__is_archived=False, 
				is_archived=False).values('model').annotate(count=Count('model'))
		elif option == None:
			query = None
		else:
			query = Scanner.objects.filter(computer__location__building_name=option,computer__location__isnull=False, 
				computer__is_archived=False, is_archived=False).values('computer__location__building_name', 
				'computer__location__office_name' ,'model').annotate(count=Count('model'))

		today = timezone.now()
		params = {
			'today': today,
			'option': option,
			'query': query,
			'request': request
		}
		return Render.render('itam/hardware/scanner/scanner-pdf-report.html', params)

class ScannerGenerateReportExpired(View):
	def get(self, request):

		query = Scanner.objects.filter(termination_status=True, is_archived=False, computer__location__isnull=False).order_by('-termination_date')

		today = timezone.now()
		params = {
			'today': today,
			'query': query,
			'request': request
		}
		return Render.render('itam/hardware/scanner/e-scanner-pdf-report.html', params)

class NetworkDeviceGenerateReport(View):
	def get(self, request):

		option = self.request.GET.get("option")
		if option == "University":
			query = NetworkDevice.objects.filter(location__isnull=False, is_archived=False).values(
				'category' ,'code').annotate(count=Count('code'))
		elif option == None:
			query = None
		else:
			query = NetworkDevice.objects.filter(location__building_name=option, location__isnull=False, is_archived=False).values(
				'location__building_name', 'location__office_name', 'code', 'category').annotate(count=Count('code'))

		today = timezone.now()
		params = {
			'today': today,
			'option': option,
			'query': query,
			'request': request
		}
		return Render.render('itam/hardware/network_device/network-device-pdf-report.html', params)

class NetworkDeviceGenerateReportExpired(View):
	def get(self, request):

		query = NetworkDevice.objects.filter(termination_status=True, is_archived=False, location__isnull=False).order_by('-termination_date')

		today = timezone.now()
		params = {
			'today': today,
			'query': query,
			'request': request
		}
		return Render.render('itam/hardware/network_device/e-network-device-pdf-report.html', params)

class SfpGenerateReport(View):
	def get(self, request):

		option = self.request.GET.get("option")
		if option == "University":
			query = Sfp.objects.filter(network_device__location__isnull=False, network_device__is_archived=False, 
				is_archived=False).values('product_number').annotate(count=Count('product_number'))
		elif option == None:
			query = None
		else:
			query = Sfp.objects.filter(network_device__location__building_name=option, network_device__location__isnull=False, 
				network_device__is_archived=False, is_archived=False).values('network_device__location__building_name', 
				'network_device__location__office_name' ,'product_number').annotate(count=Count('product_number'))

		today = timezone.now()
		params = {
			'today': today,
			'option': option,
			'query': query,
			'request': request
		}
		return Render.render('itam/hardware/sfp/sfp-pdf-report.html', params)

class SfpGenerateReportExpired(View):
	def get(self, request):

		query = Sfp.objects.filter(termination_status=True, is_archived=False, network_device__location__isnull=False).order_by('-termination_date')

		today = timezone.now()
		params = {
			'today': today,
			'query': query,
			'request': request
		}
		return Render.render('itam/hardware/sfp/e-sfp-pdf-report.html', params)

class OperatingSystemGenerateReport(View):
	def get(self, request):
		option = self.request.GET.get("option")
		if option == "University":
			query = OperatingSystem.objects.filter(computer__location__isnull=False, computer__is_archived=False, 
				is_archived=False).values('os_name').annotate(count=Count('os_name'))
		elif option == None:
			query = None
		else:
			query = OperatingSystem.objects.filter(computer__location__building_name=option,computer__location__isnull=False, 
				computer__is_archived=False, is_archived=False).values('computer__location__building_name', 
				'computer__location__office_name' ,'os_name').annotate(count=Count('os_name'))

		today = timezone.now()
		params = {
			'today': today,
			'option': option,
			'query': query,
			'request': request
		}
		return Render.render('itam/software/os/os-pdf-report.html', params)

class MicrosoftOfficeGenerateReport(View):
	def get(self, request):
		option = self.request.GET.get("option")
		if option == "University":
			query = MicrosoftOffice.objects.filter(computer__location__isnull=False, computer__is_archived=False, 
				is_archived=False).values('mo_name').annotate(count=Count('mo_name'))
		elif option == None:
			query = None
		else:
			query = MicrosoftOffice.objects.filter(computer__location__building_name=option,computer__location__isnull=False, 
				computer__is_archived=False, is_archived=False).values('computer__location__building_name', 
				'computer__location__office_name' ,'mo_name').annotate(count=Count('mo_name'))

		today = timezone.now()
		params = {
			'today': today,
			'option': option,
			'query': query,
			'request': request
		}
		return Render.render('itam/software/mo/mo-pdf-report.html', params)

class OtherSoftwareGenerateReport(View):
	def get(self, request):
		option = self.request.GET.get("option")
		if option == "University":
			query = OtherSoftware.objects.filter(computer__location__isnull=False, computer__is_archived=False, 
				is_archived=False).values('software_name').annotate(count=Count('software_name'))
		elif option == None:
			query = None
		else:
			query = OtherSoftware.objects.filter(computer__location__building_name=option,computer__location__isnull=False, 
				computer__is_archived=False, is_archived=False).values('computer__location__building_name', 
				'computer__location__office_name' ,'software_name').annotate(count=Count('software_name'))

		today = timezone.now()
		params = {
			'today': today,
			'option': option,
			'query': query,
			'request': request
		}
		return Render.render('itam/software/others/other-pdf-report.html', params)

class OtherSoftwareGenerateReportExpired(View):
	def get(self, request):

		query = OtherSoftware.objects.filter(expiration_status=True, is_archived=False, computer__location__isnull=False).order_by('-expiration_date')

		today = timezone.now()
		params = {
			'today': today,
			'query': query,
			'request': request
		}
		return Render.render('itam/software/others/e-other-pdf-report.html', params)

def location_create_popup(request):
	form = LocationForm(request.POST or None)
	if form.is_valid():
		action.send(request.user, verb='created', action_object=form.instance)
		instance = form.save()
		return HttpResponse('<script>opener.closePopup(window, "%s", "%s", "#id_location");</script>' % (instance.pk, instance))

	return render(request, "itam/location/add-location-popup.html", {"form": form})

def monitor_create_popup(request):
	form = MonitorForm(request.POST or None)
	if form.is_valid():
		instance = form.save()
		action.send(request.user, verb='created', action_object=form.instance)
		return HttpResponse('<script>opener.closePopup(window, "%s", "%s", "#id_monitor");</script>' % (instance.pk, instance))

	return render(request, "itam/hardware/monitor/add-monitor-popup.html", {"form": form})

def printer_create_popup(request):
	form = PrinterForm(request.POST or None)
	if form.is_valid():
		instance = form.save()
		action.send(request.user, verb='created', action_object=form.instance)
		return HttpResponse('<script>opener.closePopup(window, "%s", "%s", "#id_printer");</script>' % (instance.pk, instance))

	return render(request, "itam/hardware/printer/add-printer-popup.html", {"form": form})

def scanner_create_popup(request):
	form = ScannerForm(request.POST or None)
	if form.is_valid():
		instance = form.save()
		action.send(request.user, verb='created', action_object=form.instance)
		return HttpResponse('<script>opener.closePopup(window, "%s", "%s", "#id_scanner");</script>' % (instance.pk, instance))

	return render(request, "itam/hardware/scanner/add-scanner-popup.html", {"form": form})

def operating_system_create_popup(request):
	form = OperatingSystemForm(request.POST or None)
	if form.is_valid():
		instance = form.save()
		action.send(request.user, verb='created', action_object=form.instance)
		return HttpResponse('<script>opener.closePopup(window, "%s", "%s", "#id_operating_system");</script>' % (instance.pk, instance))

	return render(request, "itam/software/os/add-os-popup.html", {"form": form})

def microsoft_office_create_popup(request):
	form = MicrosoftOfficeForm(request.POST or None)
	if form.is_valid():
		instance = form.save()
		action.send(request.user, verb='created', action_object=form.instance)
		return HttpResponse('<script>opener.closePopup(window, "%s", "%s", "#id_microsoft_office");</script>' % (instance.pk, instance))

	return render(request, "itam/software/mo/add-mo-popup.html", {"form": form})

def other_software_create_popup(request):
	form = OtherSoftwareForm(request.POST or None)
	if form.is_valid():
		instance = form.save()
		action.send(request.user, verb='created', action_object=form.instance)
		return HttpResponse('<script>opener.closePopup(window, "%s", "%s", "#id_other_software");</script>' % (instance.pk, instance))

	return render(request, "itam/software/others/add-other-popup.html", {"form": form})

def network_device_create_popup(request):
	form = NetworkDeviceForm(request.POST or None)
	if form.is_valid():
		instance = form.save()
		action.send(request.user, verb='created', action_object=form.instance)
		return HttpResponse('<script>opener.closePopup(window, "%s", "%s", "#id_network_device");</script>' % (instance.pk, instance))

	return render(request, "itam/hardware/network_device/add-network-device-popup.html", {"form": form})

class BackupListView(ListView):
	model = Backup
	template_name = "itam/backups.html"
	context_object_name = "Backups"
	paginate_by = 10

	@method_decorator(user_passes_test(lambda user: user.is_admin))
	def dispatch(self, *args, **kwargs):
		return super().dispatch(*args, **kwargs)

	def get_paginate_by(self, queryset):
		return self.request.GET.get('paginate_by', self.paginate_by)

	def get_context_data(self, **kwargs):
		context = super(BackupListView, self).get_context_data(**kwargs)
		context['active_page'] = 'backup'
		context['filter_value'] = self.request.GET.get("filterInput")
		context['paginate_value'] = self.request.GET.get('paginate_by', self.paginate_by)
		return context

class BackupCreateView(CreateView):
	template_name = "itam/add-backup.html"
	model = Backup
	context_object_name = "Backups"
	form_class = UploadFileForm
	success_url = reverse_lazy('itam:backup-list')

	@method_decorator(user_passes_test(lambda user: user.is_admin))
	def dispatch(self, *args, **kwargs):
		return super().dispatch(*args, **kwargs)

	def form_valid(self, form):
		self.object = form.save()
		self.object.date_generated = datetime.datetime.now()
		self.object.file_size = self.object.backup_file.size 
		messages.success(self.request, "Backup created successfully.")
		return super().form_valid(form)

class BackupDeleteView(DeleteView):
	model = Backup
	success_url = reverse_lazy('itam:backup-list')

	def delete(self, request, *args, **kwargs):
		self.object = self.get_object()
		messages.success(self.request, "Backup file deleted successfully.")
		self.object.delete()
		return HttpResponseRedirect(self.get_success_url())

def generate_backup(request):
	today = datetime.datetime.now()
	buf = StringIO()
	call_command('dumpdata', exclude=['itam.backup'], indent=2, stdout=buf)
	a = Backup.objects.create()
	a.backup_file.save("%s.json" % str(today.strftime('%Y-%m-%d-%H%M%S')), File(buf))
	a.date_generated = today
	a.file_size = File(buf).size
	a.save()
	messages.success(request, "Backup file successfully generated.")
	return redirect('itam:backup-list')

def restore_backup(request, pk):
	f = Backup.objects.get(pk=request.POST['backup.id']).backup_file.path
	call_command('loaddata', f)
	messages.success(request, "Backup file successfully loaded.")
	return redirect('itam:backup-list')